<?php

/**
 * @package WordPress
 * @subpackage dverilending
 * @since dverilending 1.0.0
 */

class ThemeCustomizer
{
    public $options;
    private $theme_name = "dverilending";
    private $settings;

    function __construct($wp_customize)
    {
        $theme = wp_get_theme();
        $this->theme_name = "dverilending";
        $this->dverilending_customize_register($wp_customize);
    }

    public function dverilending_customize_register($wp_customize)
    {
        $this->settings = array();
        $this->init_defaults($wp_customize);
        foreach($this->options as $section_id => $section)
        {
            if($section_id != "title_tagline" && $section_id != "static_front_page") {
                $wp_customize->add_section($this->theme_name . "_" . $section_id, $section["args"]);
            }
            foreach($section["options"] as $option_id => $option)
            {
                $wp_customize->add_setting($this->theme_name.'_theme_options['.$section_id."_".$option_id.']', $option["args"]);
                $this->settings[] = array(
                    "identifier" => $this->theme_name.'_theme_options['.$section_id."_".$option_id.']',
                    "selector" => $option["selector"],
                    "modifier" => $option["modifier"],
                );
                foreach($option["controls"] as $control_args)
                {
                    if(is_array($control_args) && array_key_exists("type", $control_args))
                    {
                        if($section_id != "title_tagline" && $section_id != "static_front_page") {
                            $control_args["section"] = $this->theme_name . "_" . $section_id;
                            $control_args["settings"] = $this->theme_name . '_theme_options[' . $section_id . "_" . $option_id . ']';
                            $wp_customize->add_control($this->theme_name . "_" . $section_id . "_" . $option_id . "_field", $control_args);
                        }
                        else
                        {
                            $control_args["section"] = $section_id;
                            $control_args["settings"] = $this->theme_name . '_theme_options[' . $section_id . "_" . $option_id . ']';
                            $wp_customize->add_control($this->theme_name . "_" . $section_id . "_" . $option_id . "_field", $control_args);
                        }
                    }
                    else {
                        //echo $this->theme_name."_".$section_id." - ".$this->theme_name.'_theme_options['.$section_id."_".$option_id.']<br>';
                        //var_dump($control_args);
                        $wp_customize->add_control( new $control_args["class"]($control_args["customizer"], $control_args["name"], $control_args["args"]));
                    }
                }
            }
        }
        if(count($this->settings) > 0)
        {
            foreach($this->settings as $option)
            {
                $wp_customize->get_setting( $option["identifier"] )->transport = 'postMessage';
            }
            if ( $wp_customize->is_preview() && ! is_admin() ) { add_action( 'wp_footer', array($this, 'dverilending_customize_preview'), 21); }
        }
    }

    public function init_defaults($wp_customize)
    {
        $this->options = array(
            "static_front_page" => array(
                "options" => array(
                    "near_logo_text" => array(
                        "args" => array(
                            "default" => __('<span class="font-medium font-14">Входные двери</span><br/>
                    <span class="font-medium font-14">напрямую с завода</span>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#near_logo_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_near_logo_text]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст который будет выводиться возле логотипа', 'dverilending'),
                                    'label' => __('Текст возле логотипа', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "header_address_and_work_time" => array(
                        "args" => array(
                            "default" => __('<span class="font-medium font-14">ул.Гетьмана 10/37</span><br>
                    <span class="font-medium font-14">с 9:00 до 19:00 без выходных</span>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#header_address_and_work_time",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_header_address_and_work_time]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст который будет выводиться в поле адреса и времени работы в шапке', 'dverilending'),
                                    'label' => __('Адрес и время работы в шапке', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "recall_me" => array(
                        "args" => array(
                            "default" => __("Перезвоните мне", "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => ".recall_me_button",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись кнопки перезвонить", 'dverilending'),
                            )
                        )
                    ),
                    "phone1" => array(
                        "args" => array(
                            "default" => __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => ".phone1-link",
                        "modifier" => ".html(newval).attr('href', 'tel:' + newval.replace(/[^\d\+]/g, ''))",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Первый номер телефона", 'dverilending'),
                            )
                        )
                    ),
                    "phone2" => array(
                        "args" => array(
                            "default" => __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => ".phone2-link",
                        "modifier" => ".html(newval).attr('href', 'tel:' + newval.replace(/[^\d\+]/g, ''))",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Второй номер телефона", 'dverilending'),
                            )
                        )
                    ),
                    "phone3" => array(
                        "args" => array(
                            "default" => __('+ 3 8 <span class="font-black">(063) 630-02-20</span>', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => ".phone3-link",
                        "modifier" => ".html(newval).attr('href', 'tel:' + newval.replace(/[^\d\+]/g, ''))",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Третий номер телефона", 'dverilending'),
                            )
                        )
                    ),
                    "first_block_promo_text_come" => array(
                        "args" => array(
                            "default" => __('<span>Приходите в наш салон по адресу:</span>
                    <span>ул. Гетьмана 10/37</span>
                    <span>сообщите менеджеру промокод &#8243;двери дешево&#8243;</span>
                    <span>и получите дополнительную скидку 5%</span>
                    <span><a href="#block-13" class="map-link">Посмотреть на карте где мы находимся</a></span>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#first_block_promo_text_come",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_first_block_promo_text_come]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст приходите к нам на главной', 'dverilending'),
                                    'label' => __('Промо текст "Приходите к нам"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "door_first_line" => array(
                        "args" => array(
                            "default" => __('Взломостойкие и противопожарные', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#door_first_line",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Первая линия текста на двери", 'dverilending'),
                            )
                        )
                    ),
                    "door_second_line" => array(
                        "args" => array(
                            "default" => __('Защита от шума, запаха и холода', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#door_second_line",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Вторая линия текста на двери", 'dverilending'),
                            )
                        )
                    ),
                    "door_third_line" => array(
                        "args" => array(
                            "default" => __('Доставка по Киеву за 120 минут', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#door_third_line",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Третья линия текста на двери", 'dverilending'),
                            )
                        )
                    ),
                    "door_fourth_line" => array(
                        "args" => array(
                            "default" => __('Установка за 45 минут', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#door_fourth_line",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Четвертая линия текста на двери", 'dverilending'),
                            )
                        )
                    ),
                    "min_price" => array(
                        "args" => array(
                            "default" => __('1950', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#door_min_price",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Минимальная цена двери", 'dverilending'),
                            )
                        )
                    ),
                    "promotion_label" => array(
                        "args" => array(
                            "default" => __('Акционные предложения', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#promotion_label",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Заглавие блока \"Акционные предложения\"", 'dverilending'),
                            )
                        )
                    ),
                    "catalog_label" => array(
                        "args" => array(
                            "default" => __('Каталог межкомнатных дверей', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#catalog_label",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Заглавие Каталога", 'dverilending'),
                            )
                        )
                    ),
                    "grace_the_measurer_title" => array(
                        "args" => array(
                            "default" => __('<div>Приграсите замерщика для замера двери.</div>
                    <div>Это бесплатно (даже, если вы не будете заказывать кухню у нас)</div>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#grace_the_measurer_title",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_grace_the_measurer_title]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Заглавие блока промо "Приграсите замерщика"', 'dverilending'),
                                    'label' => __('Заглавие блока "Приграсите замерщика"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "grace_the_measurer_left_list" => array(
                        "args" => array(
                            "default" => __('<ul>
                        <li>Он правильно замерит размер кухни</li>
                        <li>Порекомендует дизайн со вкусом под стиль ремонта.</li>
                        <li>Почему важно выбрать цвет дома?</li>
                        <li>Расскажет, как выгодно использовать пространство</li>
                        <li>Подскажет, какой материа подойдёт под ваши нужды</li>
                    </ul>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#grace_the_measurer_left_list",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_grace_the_measurer_left_list]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Список слева блока промо "Приграсите замерщика"', 'dverilending'),
                                    'label' => __('Список слева блока "Приграсите замерщика"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "grace_the_measurer_right_list" => array(
                        "args" => array(
                            "default" => __('<ul>
                        <li>Привезёт образцы фасадов, чтобы вы их прощупали и увидели вживую. Какие образцы дизайнер привезёт?</li>
                        <li>Приедет в удобное для вас время</li>
                        <li>Составит дизайн проект кухни</li>
                        <li>Оформит договор в случае заказа</li>
                    </ul>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#grace_the_measurer_right_list",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_grace_the_measurer_right_list]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Список справа блока промо "Приграсите замерщика"', 'dverilending'),
                                    'label' => __('Список справа блока "Приграсите замерщика"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "grace_the_measurer_block_title" => array(
                        "args" => array(
                            "default" => __('<span><nobr>входные двери</nobr></span>
                    <span><nobr>дешевле чем в</nobr></span>
                    <span>эпицентре</span>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#grace_the_measurer_block_title",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_grace_the_measurer_block_title]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Заглавие внутри блока промо "Приграсите замерщика"', 'dverilending'),
                                    'label' => __('Заглавие блока "Приграсите замерщика"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "grace_the_measurer_block_text" => array(
                        "args" => array(
                            "default" => __('<span><nobr>Нашли дешевле?</nobr></span>
                    <span><nobr>продадим по вашей цене</nobr></span>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#grace_the_measurer_block_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_grace_the_measurer_block_text]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Контент внутри блока промо "Приграсите замерщика"', 'dverilending'),
                                    'label' => __('Контент блока "Приграсите замерщика"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "top_seven_title" => array(
                        "args" => array(
                            "default" => __('топ-7 самых надежных межкомнатных дверей', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#top_seven_title",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Заглавие блока \"Топ-7\"", 'dverilending'),
                            )
                        )
                    ),
                    "top_seven_description" => array(
                        "args" => array(
                            "default" => __('по параметру цена-качество', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#top_seven_description",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Краткое описание блока \"Топ-7\"", 'dverilending'),
                            )
                        )
                    ),
                    "over_60_models_description" => array(
                        "args" => array(
                            "default" => __('<span>Приходите в наш салон по адресу:</span>
                <span>ул. Гетьмана 10/37</span>
                <span>сообщите менеджеру промокод:</span>
                <span>"двери дешево"</span>
                <span>и получите дополнительную скидку 5%</span>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#over_60_models_description",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_over_60_models_description]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Краткое описание блока "Выбор более 60 моделей"', 'dverilending'),
                                    'label' => __('Описание блока "Выбор более 60 моделей"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "installation_for_60_min_content" => array(
                        "args" => array(
                            "default" => __('<span>Бесплатный</span>
                <span>монтаж за 60 мин</span>
                <span>мастерами с опытом от 10 лет</span>
                <div>
                    По статистике 80%  договечности работы входных дверей зависит от установки. А это очень важный шаг, потому что плохой монтаж от неопытного "мастера" ничего хорошего не сулит и могут образоваться щели между дверью и коробом, будет продувание, слабая шумоизоляция, а также плохо работать замочная система.
                </div>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#installation_for_60_min_content",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_installation_for_60_min_content]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Весь контент блока "монтаж за 60 мин"', 'dverilending'),
                                    'label' => __('Контент блока "монтаж за 60 мин"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "delivery_first_line" => array(
                        "args" => array(
                            "default" => __('доставка по Киеву', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#delivery_first_line",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Первая линия блока \"доставка\"", 'dverilending'),
                            )
                        )
                    ),
                    "delivery_second_line" => array(
                        "args" => array(
                            "default" => __('за 120 минут', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#delivery_second_line",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Вторая линия блока \"доставка\"", 'dverilending'),
                            )
                        )
                    ),
                    "about_company_title" => array(
                        "args" => array(
                            "default" => __('О компании ″викоцентр″', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#about_company_title",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Заглавие блока \"О компании\"", 'dverilending'),
                            )
                        )
                    ),
                    "about_company_first_text" => array(
                        "args" => array(
                            "default" => __('15 лет с 2001', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#about_company_first_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись первого изображения блока  \"О компании\"", 'dverilending'),
                            )
                        )
                    ),
                    "about_company_second_text" => array(
                        "args" => array(
                            "default" => __('600 моделей дверей', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#about_company_second_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись второго изображения блока  \"О компании\"", 'dverilending'),
                            )
                        )
                    ),
                    "about_company_third_text" => array(
                        "args" => array(
                            "default" => __('600 моделей дверей', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#about_company_third_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись третьего изображения блока  \"О компании\"", 'dverilending'),
                            )
                        )
                    ),
                    "about_company_fourth_text" => array(
                        "args" => array(
                            "default" => __('Установлено 7268 дверей', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#about_company_fourth_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись четвертого изображения блока  \"О компании\"", 'dverilending'),
                            )
                        )
                    ),
                    "about_company_year" => array(
                        "args" => array(
                            "default" => __('за 2015 год', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#about_company_year",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись \"за 20хх год\" блока \"О компании\"", 'dverilending'),
                            )
                        )
                    ),
                    "about_company_doors_count" => array(
                        "args" => array(
                            "default" => __('1163 двери куплено в нашей компании по Киеву', "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#about_company_doors_count",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись \"xxxx двери куплено в нашей компании по Киеву\" блока \"О компании\"", 'dverilending'),
                            )
                        )
                    ),
                    "google_map_info" => array(
                        "args" => array(
                            "default" => __('Приходите в наш салон по адресу:<br> ул. Гетьмана 10/37<br> сообщите менеджеру промокод:<br> "двери дешево"<br> и получите дополнительную скидку 5%', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#google_map_info",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_google_map_info]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст промо блока гугл карты', 'dverilending'),
                                    'label' => __('Текст блока гугл карты', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "google_map_link" => array(
                        "args" => array(
                            "default" => "https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%92%D0%B0%D0%B4%D0%B8%D0%BC%D0%B0+%D0%93%D0%B5%D1%82%D1%8C%D0%BC%D0%B0%D0%BD%D0%B0,+10,+%D0%9A%D0%B8%D0%B5%D0%B2",
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#google_map_link",
                        "modifier" => ".attr('href', newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Ссылка на гугл карты \"Где мы находимся\"", 'dverilending'),
                            )
                        )
                    ),
                    "recomended_links" => array(
                        "args" => array(
                            "default" => __('<div>Также рекомендуем посетить:</div>
                <a href="http://dverivh.com.ua">dverivh.com.ua</a>
                <a href="http://okna.com.ua">okna.com.ua</a>
                <a href="http://sma.com.ua">sma.com.ua</a>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#recomended_links",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_recomended_links]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Блок "Рекомендуемые ссылки"', 'dverilending'),
                                    'label' => __('Блок "Рекомендуемые ссылки"', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "footer_address" => array(
                        "args" => array(
                            "default" => __('<div>ул. Гетьмана 10/37</div>
                    <div>с 9:00 до 19:00 без выходных</div>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#footer_address",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_footer_address]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст адреса в подвале', 'dverilending'),
                                    'label' => __('Адрес в подвале', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "zabotri" => array(
                        "args" => array(
                            "default" => __('<div>ул. Гетьмана 10/37</div>
                    <div>с 9:00 до 19:00 без выходных</div>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#zabotri",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_zabotri]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст рекомендуемых ресурсов в модальном окне "Вам перезвонят через 25 секунд"', 'dverilending'),
                                    'label' => __('Текст рекомендуемых ресурсов в модальном окне', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "balckon" => array(
                        "args" => array(
                            "default" => __('Застекливание и утепление балконов<br><a href="http://balkoni.com.ua">balkoni.com.ua</a>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#balckon",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_balckon]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст рекомендуемых ресурсов второй блок в модальном окне "Вам перезвонят через 25 секунд"', 'dverilending'),
                                    'label' => __('Текст рекомендуемых ресурсов второй блок в модальном окне', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "okna" => array(
                        "args" => array(
                            "default" => __('Изготовление и установка окон<br><a href="http://okna.com.ua">okna.com.ua</a>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#okna",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_okna]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст рекомендуемых ресурсов третий блок в модальном окне "Вам перезвонят через 25 секунд"', 'dverilending'),
                                    'label' => __('Текст рекомендуемых ресурсов третий блок в модальном окне', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    ),
                    "doors" => array(
                        "args" => array(
                            "default" => __('Изготовление и установка межкомнатных дверей<br><a href="http://dveri.com.ua">dveri.com.ua</a>', 'dverilending'),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#doors",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "name" => $this->theme_name . '_theme_options[static_front_page_doors]',
                                "class" => "Text_Editor_Custom_Control",
                                "customizer" => $wp_customize,
                                "args" => array(
                                    'description' => __('Текст рекомендуемых ресурсов четвертый блок в модальном окне "Вам перезвонят через 25 секунд"', 'dverilending'),
                                    'label' => __('Текст рекомендуемых ресурсов четвертый блок в модальном окне', 'dverilending'),
                                    'section' => 'static_front_page',
                                )
                            )
                        )
                    )
                )
            ),
            "mm_product_section" => array(
                "args" => array(
                    "title" => __("Настройки страницы товара", 'dverilending'),
                    "priority" => 122,
                ),
                "options" => array(
                    "delivery_text" => array(
                        "args" => array(
                            "default" => __("Бесплатная доставка за 120 минут", "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#delivery_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись о доставке", 'dverilending'),
                            )
                        )
                    ),
                    "mounting_text" => array(
                        "args" => array(
                            "default" => __("Бесплатный монтаж за 60 минут", "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#mounting_text",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись о монтаже", 'dverilending'),
                            )
                        )
                    ),
                    "how_buy_delivery" => array(
                        "args" => array(
                            "default" => __("В течении 120 минут двери доставляют к вам по указаному адресу", "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#how_buy_delivery",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись о доставке", 'dverilending'),
                            )
                        )
                    ),
                    "how_buy_mounting" => array(
                        "args" => array(
                            "default" => __("Еще через 60 минут двери уже будут установлены", "dverilending"),
                            "type" => "option",
                            "capability" => "edit_theme_options",
                        ),
                        "selector" => "#how_buy_mounting",
                        "modifier" => ".html(newval)",
                        "controls" => array(
                            array(
                                "type" => "text",
                                "label" => __("Надпись о доставке", 'dverilending'),
                            )
                        )
                    ),
                )
            )
        );
    }

    public function dverilending_customize_preview()
    {
        if(count($this->settings) > 0)
        {
            ?>
            <script type="text/javascript">
                (function($) {

                    <?php
                    foreach($this->settings as $option):
                    ?>
                    wp.customize('<?php echo $option["identifier"] ?>', function(value) {
                        value.bind(function(newval) {
                            $('<?php echo $option["selector"] ?>')<?php echo $option["modifier"] ?>;
                        });
                    });
                    <?php
                    endforeach;
                    ?>

                })(jQuery);
            </script>
            <?php
        }
    }

    public static function get_theme_setting($setting, $default = "")
    {
        $all_settings = get_option("dverilending_theme_options");
        $result = $default;
        if($all_settings !== false && is_array($all_settings) && array_key_exists($setting, $all_settings))
        {
            $result = $all_settings[$setting];
        }
        return $result;
    }

}
