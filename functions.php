<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

require get_stylesheet_directory().'/theme_class/Text_Editor_Custom_Control.php';
require get_stylesheet_directory().'/plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/mmwebstudioteam/dveri-landing/',
    __FILE__,
    'dverilending'
);

$theme_options  = get_option('true_options');

$myUpdateChecker->setAuthentication($theme_options["update_token"]);

$myUpdateChecker->setBranch('master');

require 'theme_class/theme-customizer.php';

$customizer;

function init_customize_register($wp_customize)
{
    $customizer = new ThemeCustomizer($wp_customize);
}

add_action('customize_register', 'init_customize_register');

/**
 * Assign the Storefront version to a var
 */
require 'theme_class/Mobile_Detect.php';

$theme              = wp_get_theme( 'dverilanding' );
$storefront_version = $theme['Version'];
global $device;
$device = new Mobile_Detect();

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$dverilanding = (object) array(
	'version'    => $dverilanding_version,

);
function filter_woocommerce_product_get_image( $image, $_this, $size, $attr, $placeholder ) {
    global $products_is_ajax_load;
    if(!$products_is_ajax_load) {
        $image = str_replace('src=', 'class="lazy-load" src="' . get_stylesheet_directory_uri() . '/img/ajax-loader.gif" data-src=', $image);
    }
    return $image;
}
add_filter ( 'woocommerce_product_get_image', 'filter_woocommerce_product_get_image', 10, 5);

add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'woocommerce' );

add_action( 'after_setup_theme', 'theme_register_main_menu' );

add_action( 'dveri_product_tumbnails', 'woocommerce_template_loop_product_thumbnail' );

remove_filter('woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories');

add_action( 'wp_enqueue_scripts', 'theme_name_scripts', 50 );
function theme_name_scripts() {
    wp_deregister_script('twentysixteen-skip-link-focus-fix');
    wp_deregister_style('twentysixteen-style');
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri().'/bootstrap/css/bootstrap.min.css', array(), '3.7.0' );
    wp_enqueue_style( 'bootstrap-slider', get_stylesheet_directory_uri().'/bootstrap/css/bootstrap-slider.min.css', array('bootstrap'), '1.1.0' );
    wp_enqueue_style( 'lazyload', get_stylesheet_directory_uri().'/css/lazyload.min.css', array('bootstrap'), '1.0.0' );
    wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array('bootstrap-slider'), '4.7.0' );
    wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/bootstrap/js/bootstrap.min.js', array('jquery'), '3.7.0', true );
    wp_enqueue_script( 'lazyload', get_stylesheet_directory_uri() . '/js/lazyload.min.js', array('jquery'), '1.1.0', true );
    wp_enqueue_script( 'bootstrap-slider', get_stylesheet_directory_uri() . '/bootstrap/js/bootstrap-slider.min.js', array('bootstrap'), '10.6.1', true );
    wp_enqueue_script( 'multiselect', get_stylesheet_directory_uri() . '/js/multiselect.js', array('bootstrap'), '1.0.1', true );
    wp_enqueue_script( 'circle-progress', get_stylesheet_directory_uri() . '/js/circle-progress.min.js', array('bootstrap'), '1.0.0', true );
}

function theme_register_main_menu() {
    load_theme_textdomain('dverilending', get_stylesheet_directory().'/languages');
    register_nav_menu( 'primary', __('Главное меню', "dverilending") );
}

function show_main_menu()
{
    wp_nav_menu( array(
        'theme_location'  => 'primary',
        'container'       => false,
        'menu_class'      => 'nav',
        'echo'            => true,
        'fallback_cb'     => '__return_empty_string',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           => 0,
    ) );
}

function show_main_mobile_menu()
{
wp_nav_menu( array(
    'theme_location'  => 'primary',
    'container'       => false,
    'menu_class'      => 'menu',
    'echo'            => true,
    'fallback_cb'     => '__return_empty_string',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth'           => 0,
) );
}

function featured_products_orderby( $orderby, $query ) {
    global $wpdb;

    if ( 'featured_products' == $query->get( 'orderby' ) || (is_array($query->get( 'orderby' )) && array_key_exists("featured_products", $query->get( 'orderby' )))) {
        $featured_product_ids = (array) wc_get_featured_product_ids();
        if ( count( $featured_product_ids ) ) {
            $string_of_ids = '(' . implode( ',', $featured_product_ids ) . ')';
            $orderby       = "( {$wpdb->posts}.ID IN {$string_of_ids} ) " . $query->get( 'order' );
        }
    }

    return $orderby;
}
add_filter( 'posts_orderby', 'featured_products_orderby', 10, 2 );

function woocommerce_content() {
    global $products_is_ajax_load;
    $products_is_ajax_load = false;
    if ( is_singular( 'product' ) ) {

        while ( have_posts() ) :
            the_post();
            wc_get_template_part( 'content', 'single-product' );
        endwhile;

    } else {
        ?>

        <?php if ( woocommerce_product_loop() ) : ?>

            <?php
                    $loop = new WP_Query(array(
                        'post_type' => 'product',  // указываем, что выводить нужно именно товары
                        'posts_per_page' => 12, // количество товаров для отображения
                        'orderby'        => array(
                                "featured_products" => "DESC",
                                "date" => "DESC"
                        ), // тип сортировки (в данном случае по дате)
                    ));
                    if (wc_get_loop_prop('total')) :
                        $range = wpp_get_extremes_price_in_products();
                        global $price_range;
                        $price_range = $range;
                        ?>
                    <form id="filters" method="post" class="good-filter-form"  data-page="1">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 bordered-table">
                                    <?php
                                    $theme_options  = get_option('true_options');
                                    ?>
                                    <?php
                                    if(!empty($theme_options['type_filter']))
                                    {
                                        $types = acf_get_fields_by_id($theme_options['type_filter']);
                                        ?>
                                        <label for = "type"><?php echo $types[0]['label'] ?></label>
                                        <select id="type" name="<?php echo $types[0]['name'] ?>" class="multiselect-ui form-control static-filter" data-placeholder="<?php esc_attr_e("Ничего не выбрано", "dverilending") ?>" multiple="multiple">
                                            <?php
                                            foreach ($types[0]['choices'] as $key => $label)
                                            {
                                                ?>
                                                <option value="<?php echo $key ?>"><?php echo $label ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 bordered-table">
                                    <?php
                                    if(!empty($theme_options['seria_filter']))
                                    {
                                        $serias = acf_get_fields_by_id($theme_options['seria_filter']);
                                        ?>
                                        <label for = "seria"><?php echo $serias[0]['label'] ?></label>
                                        <select id="seria" name="<?php echo $serias[0]['name'] ?>" class="multiselect-ui form-control static-filter" data-placeholder="<?php esc_attr_e("Ничего не выбрано", "dverilending") ?>" multiple="multiple">
                                            <?php
                                            foreach ($serias[0]['choices'] as $key => $label)
                                            {
                                                ?>
                                                <option value="<?php echo $key ?>"><?php echo $label ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-6 bordered-table price-filter">
                                    <?php echo sprintf(__('<span class="font-bold font-24 min-current-price">%s</span>грн.', 'dverilending'), $price_range->min_price) ?> <input class="price-slider" name="price_range" id="price_filter" type="text" class="span2" value="" data-slider-min="<?php echo $price_range->min_price ?>" data-slider-max="<?php echo $price_range->max_price ?>" data-slider-tooltip="hide" data-slider-step="5" data-slider-value="[<?php echo $price_range->min_price ?>,<?php echo $price_range->max_price ?>]"/> <?php echo sprintf(__('<span class="font-bold font-24 max-current-price">%s</span>грн.', 'dverilending'), $price_range->max_price) ?>
                                </div>
                                <?php
                                $orher_filters = $theme_options['cutom_filters'];
                                if(!empty($orher_filters)) {
                                    $orher_filters = explode('&', $orher_filters);
                                    foreach ($orher_filters as $filter) {
                                        $filter = explode(':', $filter);
                                        $filter_fields = acf_get_fields_by_id($filter[1]);
                                        foreach ($filter_fields as $field) {
                                            ?>
                                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 bordered-table">
                                                <label for="<?php echo $field['name'] ?>"><?php echo $field['label'] ?></label>
                                                <select id="<?php echo $field['name'] ?>"
                                                        name="<?php echo $field['name'] ?>"
                                                        class="multiselect-ui form-control static-filter"
                                                        data-placeholder="<?php esc_attr_e("Ничего не выбрано", "dverilending") ?>" multiple="multiple">
                                                    <?php
                                                    foreach ($field['choices'] as $key => $label) {
                                                        ?>
                                                        <option value="<?php echo $key ?>"><?php echo $label ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        <?php }
                                    }
                                } ?>
                            </div>
                        </div>
                    </form>
                    <script type="text/javascript">
                        jQuery(function($) {
                            $(document).ready(function () {
                                price_filter = new Slider('#price_filter');
                                $("#price_filter").on("change", function (event)
                                {
                                    $(".min-current-price").html(event.value.newValue[0]);
                                    $(".max-current-price").html(event.value.newValue[1]);
                                })
                                $("#price_filter").on('slideStop', function (new_range) {
                                    $(this).closest('form').submit();
                                });
                            });
                        });
                    </script>
                        <?php woocommerce_product_loop_start(); ?>
                        <?php while ($loop->have_posts()) : ?>
                            <?php $loop->the_post(); ?>
                            <?php wc_get_template_part('content', 'product'); ?>
                        <?php endwhile; ?>
                        <?php woocommerce_product_loop_end(); ?>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>


                    <?php do_action('woocommerce_after_shop_loop'); ?>
                    <div class="other-models-greeting text-center"><a class="text-white load-more"  data-page="2" href = "javascript: void (0)"><span><?php esc_html_e("Другие модели", "dverilending"); ?></span></a></div>
                    <?php
            else : ?>

            <?php do_action( 'woocommerce_no_products_found' ); ?>

        <?php
        endif;

    }
}

add_action('wp_ajax_loadmore', 'true_load_products');
add_action('wp_ajax_nopriv_loadmore', 'true_load_products');

function true_load_products()
{
    global $products_is_ajax_load;
    $products_is_ajax_load = true;
    $filters = $_POST['filters'];
    $price_range = $filters['price_range'];
    unset($filters['price_range']);
    $min_price = null; $max_price = null;
    $meta_query['relation'] = 'AND';
    if(!empty($price_range))
    {
        list($min_price, $max_price) = explode(',', $price_range);
        $meta_query[] = array(
            'key' => '_price',
            'value' => $min_price,
            'type' => 'numeric',
            'compare' => '>='
        );
        $meta_query[] = array(
            'key' => '_price',
            'value' => $max_price,
            'type' => 'numeric',
            'compare' => '<='
        );
    }
    foreach ($filters as $key => $filter)
    {
        $meta_query[] = array(
            'key' => $key,
            'value' => $filter,
            'compare' => 'IN'
        );
    }
    $page = intval($_POST['page']);
    $loop = new WP_Query(array(
        'post_type' => 'product',  // указываем, что выводить нужно именно товары
        'paged' => $page,  // номер страницы
        'posts_per_page' => 12, // количество товаров для отображения
        'orderby' => 'date', // тип сортировки (в данном случае по дате)
        'post_status' => 'publish', // получаем только опубликованые
        'meta_query' => $meta_query
    ));
    ?>
    <?php
    ob_start();
    while ($loop->have_posts()) : ?>
        <?php $loop->the_post(); ?>
        <?php wc_get_template_part('content', 'product'); ?>
    <?php endwhile;
    $html = ob_get_clean();
    $data['html'] = $html;
    $data['max_page'] = $loop->max_num_pages;
    echo json_encode($data);
    ?>
<?php wp_reset_postdata();
die();
}


$true_page = 'theme_options.php'; // это часть URL страницы, рекомендую использовать строковое значение, т.к. в данном случае не будет зависимости от того, в какой файл вы всё это вставите

/*
 * Функция, добавляющая страницу в пункт меню Настройки
 */
function true_options() {
    global $true_page;
    add_theme_page( 'Параметры темы', 'Параметры темы', 'edit_theme_options', $true_page, 'true_option_page');
}
add_action('admin_menu', 'true_options');

/**
 * Возвратная функция (Callback)
 */
function true_option_page(){
    global $true_page;
    ?><div class="wrap">
    <h2>Параметры темы</h2>
    <form method="post" enctype="multipart/form-data" action="options.php">
        <?php
        settings_fields('true_options'); // меняем под себя только здесь (название настроек)
        do_settings_sections($true_page);
        ?>
        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
        </p>
    </form>
    </div><?php
}

/*
 * Регистрируем настройки
 * Мои настройки будут храниться в базе под названием true_options (это также видно в предыдущей функции)
 */
function true_option_settings() {
    global $true_page;
    // Присваиваем функцию валидации ( true_validate_settings() ). Вы найдете её ниже
    register_setting( 'true_options', 'true_options', 'true_validate_settings' ); // true_options

    // Добавляем секцию
    add_settings_section( 'true_section_1', 'Настройки телеграм', '', $true_page );
    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'block_1_fist_line',
        'label_for' => 'block_1_fist_line'
    );
    add_settings_field( 'block_1_fist_line_field', 'Первая строка первого блока', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'block_1_second_line',
        'label_for' => 'block_1_second_line'
    );
    add_settings_field( 'block_1_second_line_field', 'Вторая строка первого блока', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'block_1_third_line',
        'label_for' => 'block_1_third_line'
    );
    add_settings_field( 'block_1_third_line_field', 'Третья строка первого блока', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'block_1_four_line',
        'label_for' => 'block_1_four_line'
    );
    add_settings_field( 'block_1_four_line_field', 'Четвертая строка первого блока', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    // Создадим текстовое поле в первой секции
    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'bot_token',
        'label_for' => 'bot_token' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'bot_token_field', 'Токен бота', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'chat_id',
        'label_for' => 'chat_id' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'chat_id_field', 'ID канала', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    add_settings_section( 'theme_settings', 'Настройки Темы', '', $true_page );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'notify_email',
        'label_for' => 'notify_email' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'notify_email_field', 'Email для уведомлений о заказе', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );
    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'copyright_start_year',
        'desc'      => 'Если не указано то в футере будет выводиться текущий год, если указать то в футере будет выводить в формате "начальный_год-текущий_год"',
        'label_for' => 'copyright_start_year' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'copyright_start_year_field', 'Год начала копирайтинга', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );
    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'link_fb',
        'label_for' => 'link_fb'
    );
    add_settings_field( 'link_fb_field', 'Ссылка Facebook', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'link_ins',
        'label_for' => 'link_ins'
    );
    add_settings_field( 'link_ins_field', 'Ссылка Instagram', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'type_filter',
        'label_for' => 'type_filter' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'type_filter_field', 'Первый статический фильтр', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'seria_filter',
        'label_for' => 'seria_filter' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'seria_filter_field', 'Второй статический фильтр', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );

    $true_field_params = array(
        'type'      => 'text', // тип
        'id'        => 'cutom_filters',
        'label_for' => 'cutom_filters' // позволяет сделать название настройки лейблом (если не понимаете, что это, можете не использовать), по идее должно быть одинаковым с параметром id
    );
    add_settings_field( 'cutom_filters_field', 'Список фильров', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );

    $true_field_params = array(
        'type'      => 'password', // тип
        'id'        => 'update_token',
        'label_for' => 'update_token'
    );
    add_settings_field( 'update_token_field', 'Ключ обновлений темы', 'true_option_display_settings', $true_page, 'theme_settings', $true_field_params );
}
add_action( 'admin_init', 'true_option_settings' );

/*
 * Функция отображения полей ввода
 * Здесь задаётся HTML и PHP, выводящий поля
 */
function true_option_display_settings($args) {
    extract( $args );

    $option_name = 'true_options';

    $o = get_option( $option_name );

    switch ( $type ) {
        case 'password':
            $o[$id] = esc_attr( stripslashes($o[$id]) );
            echo "<input class='regular-text' type='password' id='$id' name='" . $option_name . "[$id]' value='$o[$id]' />";
            echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
            break;
        case 'text':
            $o[$id] = esc_attr( stripslashes($o[$id]) );
            echo "<input class='regular-text' type='text' id='$id' name='" . $option_name . "[$id]' value='$o[$id]' />";
            echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
            break;
        case 'textarea':
            $o[$id] = esc_attr( stripslashes($o[$id]) );
            echo "<textarea class='code large-text' cols='50' rows='10' type='text' id='$id' name='" . $option_name . "[$id]'>$o[$id]</textarea>";
            echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
            break;
        case 'checkbox':
            $checked = ($o[$id] == 'on') ? " checked='checked'" :  '';
            echo "<label><input type='checkbox' id='$id' name='" . $option_name . "[$id]' $checked /> ";
            echo ($desc != '') ? $desc : "";
            echo "</label>";
            break;
        case 'select':
            echo "<select id='$id' name='" . $option_name . "[$id]'>";
            foreach($vals as $v=>$l){
                $selected = ($o[$id] == $v) ? "selected='selected'" : '';
                echo "<option value='$v' $selected>$l</option>";
            }
            echo ($desc != '') ? $desc : "";
            echo "</select>";
            break;
        case 'radio':
            echo "<fieldset>";
            foreach($vals as $v=>$l){
                $checked = ($o[$id] == $v) ? "checked='checked'" : '';
                echo "<label><input type='radio' name='" . $option_name . "[$id]' value='$v' $checked />$l</label><br />";
            }
            echo "</fieldset>";
            break;
    }
}

/*
 * Функция проверки правильности вводимых полей
 */
function true_validate_settings($input) {
    foreach($input as $k => $v) {
        $valid_input[$k] = trim($v);

        /* Вы можете включить в эту функцию различные проверки значений, например
        if(! задаем условие ) { // если не выполняется
            $valid_input[$k] = ''; // тогда присваиваем значению пустую строку
        }
        */
    }
    return $valid_input;
}

// define the wpcf7_mail_components callback
function filter_wpcf7_mail_components( $components, $wpcf7_get_current_contact_form, $instance ) {
    $message=$components['body'];
    $telegram = get_option('true_options');
    $message = urlencode($message);
    file_get_contents('https://api.telegram.org/bot'.$telegram['bot_token'].'/sendMessage?chat_id='.$telegram['chat_id'].'&text='.$message);
    return $components;
};

// add the filter
add_filter( 'wpcf7_mail_components', 'filter_wpcf7_mail_components', 10, 3 );

/**
* Получает экстремальнве значения ценв в категории товара
* @param $term_id - id категории товара
*
   * @return mixed
    */
  function wpp_get_extremes_price_in_products() {

      global $wpdb;
      $sql = "
      SELECT  MIN(CAST(meta_value AS UNSIGNED INTEGER)) as min_price , MAX(CAST(meta_value AS UNSIGNED INTEGER)) as max_price
      FROM {$wpdb->posts} 
      INNER JOIN {$wpdb->term_relationships} ON ({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)
      INNER JOIN {$wpdb->postmeta} ON ({$wpdb->posts}.ID = {$wpdb->postmeta}.post_id) 
      WHERE {$wpdb->posts}.post_type = 'product' 
      AND {$wpdb->posts}.post_status = 'publish' 
      AND {$wpdb->postmeta}.meta_key = '_price'
      ";

      $result = $wpdb->get_results( $wpdb->prepare( $sql, array()) );

      return $result[ 0 ];

  }