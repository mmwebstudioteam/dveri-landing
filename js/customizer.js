( function( $ ) {
    wp.customizerCtrlEditor = {

        init: function() {

            $(window).load(function(){
                var adjustArea = $('textarea.wp-editor-area');
                adjustArea.each(function(){
                    var tArea = $(this),
                        id = tArea.attr('id'),
                        data_customize_setting_link = tArea.attr('data-customize-setting-link'),
                        input = $('textarea[data-customize-setting-link="'+ data_customize_setting_link +'"]'),
                        editor = tinyMCE.get(id),
                        setChange,
                        content;

                    if(editor){
                        editor.onChange.add(function (ed, e) {
                            ed.save();
                            content = editor.getContent();
                            clearTimeout(setChange);
                            setChange = setTimeout(function(){
                                input.val(content).trigger('change');
                            },500);
                        });
                    }

                    tArea.css({
                        visibility: 'visible'
                    }).on('keyup', function(){
                        content = tArea.val();
                        clearTimeout(setChange);
                        setChange = setTimeout(function(){
                            input.val(content).trigger('change');
                        },500);
                    });
                });
            });
        }

    };

    wp.customizerCtrlEditor.init();

} )( jQuery );