<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head();
    global $device_type;
    $osType = '';
    if($device->isTablet())
    {
        $device_type = 'mobile';
    }
    else if($device->isMobile())
    {
        $device_type = 'mobile';
    }
    else if(!$device->isTablet() && !$device->isMobile())
    {
        $device_type = 'desktop';
    }
    if($device->isiPad())
    {
        $osType = ' ipad';
    }
    else if($device->isiPhone())
    {
        $osType = ' iphone';
    }
    else if($device->isAndroidOS())
    {
        $osType = ' android';
    }
    else if(!$device->isWindowsPhoneOS())
    {
        $osType = ' windows-phone';
    }
    else if(!$device->isWindowsMobileOS())
    {
        $osType = ' windows-mobile';
    }
    ?>
</head>

<body class="<?php echo $device_type.$osType ?>">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>
        <?php
        if(!array_key_exists('submit_order', $_POST))
        {
            ?>
        <div class = "my-20px">
            <div class="container clearfix">
                <div class="footer-about">

                    <div class="copyright">
                        © <?php $theme_options  = get_option('true_options'); echo !empty($theme_options['copyright_start_year']) && is_numeric($theme_options['copyright_start_year']) ? $theme_options['copyright_start_year'].'-'.date('Y') : date('Y') ?> <?php bloginfo('name') ?>. <?php esc_html_e("Все права защищены", "dverilending")?>
                    </div>

                </div>


            </div>
        </div>
        <?php
        }
        ?>
        <?php wp_footer(); ?>
</body>
</html>


