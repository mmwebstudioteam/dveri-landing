<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
global $device;
$theme_options  = get_option('true_options');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head();
    global $device_type;
    $osType = '';
    if($device->isTablet())
    {
        $device_type = 'mobile';
    }
    else if($device->isMobile())
    {
        $device_type = 'mobile';
    }
    else if(!$device->isTablet() && !$device->isMobile())
    {
        $device_type = 'desktop';
    }
    if($device->isiPad())
    {
        $osType = ' ipad';
    }
    else if($device->isiPhone())
    {
        $osType = ' iphone';
    }
    else if($device->isAndroidOS())
    {
        $osType = ' android';
    }
    else if($device->isWindowsPhoneOS())
    {
        $osType = ' windows-phone';
    }
    else if($device->isWindowsMobileOS())
    {
        $osType = ' windows-mobile';
    }
    ?>
    <script type="text/javascript">
        var price_filter;
        var promTimer;
    </script>
</head>

<body class="<?php echo $device_type.$osType ?>">

<div id="wrapper">

    <div id="block-1">
        <div class="container clearfix">

            <div class="top-head clearfix">
                <div class="logo fl-left inl-block">
                    <img src="/wp-content/themes/dverilending/img/logo.png" alt="">
                </div>
                <div id="near_logo_text" class="inl-block fl-left">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_near_logo_text", __('<span class="font-medium font-14">Входные двери</span><br/>
                    <span class="font-medium font-14">напрямую с завода</span>', 'dverilending')) ?>
                </div>

                <div id="header_address_and_work_time" class="cntr hd-cntr inl-block">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_header_address_and_work_time", __('<span class="font-medium font-14">ул.Гетьмана 10/37</span><br>
                    <span class="font-medium font-14">с 9:00 до 19:00 без выходных</span>', 'dverilending')) ?>
                </div>
                <div class="fl-right inl-block callback">
                    <span class="recall_me_button" onclick="open_modal_call('head')"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_recall_me", __("Перезвоните мне", "dverilending")) ?></span>
                </div>
                <div class="fl-right inl-block">
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone1-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></a><br>
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone2-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a>
                </div>
                <div class="float-right mobile-panel-header">
                    <div class="phone-menu-container">
                        <a data-toggle="modal" data-target="#phone_modal"><img src="/wp-content/themes/dverilending/img/mobile-phone.png"></a>
                    </div>
                    <div class="mobile-menu-container">
                        <a data-toggle="modal" data-target="#mobile_menu_modal"><img src="/wp-content/themes/dverilending/img/mobile-menu.png"></a>
                    </div>
                </div>
                    <div class="modal fade" id="phone_modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body text-center">
                                    <ul class="menu">
                                        <li><a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone1-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></a></li>
                                        <li><a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14  phone2-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a></li>
                                        <li><a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone3", __('+ 3 8 <span class="font-black">(063) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14  phone3-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone3", __('+ 3 8 <span class="font-black">(063) 630-02-20</span>', "dverilending")) ?></a></li>
                                    </ul>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="modal fade" id="mobile_menu_modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body text-center">
                                <div class="inl-block callback">
                                    <span class="recall_me_button" onclick="open_modal_call('menu')"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_recall_me", __("Перезвоните мне", "dverilending")) ?></span>
                                </div>
                                <?php show_main_mobile_menu() ?>
                                <button type="button" class="btn btn-default dismiss-modal" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
                    <script type="text/javascript">
                        jQuery(function($) {
                            $(document).ready(function () {
                                $('#mobile_menu_modal ul.menu a').click(function()
                                {
                                    $('#mobile_menu_modal').modal('hide');
                                });
                            });
                        });
                    </script>
            </div>

            <?php
            if(!empty($theme_options['block_1_second_line']) && $device_type == 'mobile')
            {
                $text_parts = explode(' ', $theme_options['block_1_second_line']);
                $new_text = '';
                for($num = 0; $num < count($text_parts); $num++)
                {
                    if($num < count($text_parts) - 1)
                    {
                        $new_text .= '<span class = "mobile-background">'.$text_parts[$num].' </span>';
                    }
                    else
                    {
                        $new_text .= '<span class = "mobile-background">'.$text_parts[$num].'</span>';
                    }
                }
                $theme_options['block_1_second_line'] = $new_text;
            }

            if(!empty($theme_options['block_1_third_line']) && $device_type == 'mobile')
            {
                $text_parts = explode(' ', $theme_options['block_1_third_line']);
                $new_text = '';
                for($num = 0; $num < count($text_parts); $num++)
                {
                    if($num < count($text_parts) - 1)
                    {
                        $new_text .= '<span class = "mobile-background">'.$text_parts[$num].' </span>';
                    }
                    else
                    {
                        $new_text .= '<span class = "mobile-background">'.$text_parts[$num].'</span>';
                    }
                }
                $theme_options['block_1_third_line'] = $new_text;
            }
            ?>
            <div class="left fl-left">
                <div class="four-mn">
                    <span class="font-light font-40"><?php echo !empty($theme_options['block_1_fist_line']) ? $theme_options['block_1_fist_line'] : '' ?></span><br>
                    <span class="font-black font-72"><b><?php echo !empty($theme_options['block_1_second_line']) ? $theme_options['block_1_second_line'] : '' ?></b></span><br>
                    <span class="font-medium font-50"><?php echo !empty($theme_options['block_1_third_line']) ? $theme_options['block_1_third_line'] : '' ?></span><br>
                    <span class="font-light font-40"><?php echo !empty($theme_options['block_1_four_line']) ? $theme_options['block_1_four_line'] : '' ?></span>
                </div>
                <a href="#block-4">
                    <div class='choise-door'>
                        <span class="font-black font-24">Выбрать дверь</span>
                    </div></a>
                <div id="first_block_promo_text_come" class='hdr-text'>
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_first_block_promo_text_come", __('<span>Приходите в наш салон по адресу:</span>
                    <span>ул. Гетьмана 10/37</span>
                    <span>сообщите менеджеру промокод &#8243;двери дешево&#8243;</span>
                    <span>и получите дополнительную скидку 5%</span>
                    <span><a href="#block-13" class="map-link">Посмотреть на карте где мы находимся</a></span>', "dverilending")) ?>
                </div>
                <div class='hdr-line'></div>
            </div>

            <div class="right fl-right">
                <div class="four-undrln">
                    <div class="undrln"><span class="font-light font-20" id="door_first_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_door_first_line", __('Взломостойкие и противопожарные', "dverilending")) ?></span></div>
                    <div class="undrln"><span class="font-light font-20" id="door_second_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_door_second_line", __('Защита от шума, запаха и холода', "dverilending")) ?></span></div>
                    <div class="undrln"><span class="font-light font-20" id="door_third_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_door_third_line", __('Доставка по Киеву за 120 минут', "dverilending")) ?></span></div>
                    <div class="undrln"><span class="font-light font-20" id="door_fourth_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_door_fourth_line", __('Установка за 45 минут', "dverilending")) ?></span></div>
                    <div class="hdr-price">
                        <?php echo sprintf(__("<span class='font-black font-30'>От</span>
                        <span class='font-black font-60' id='door_min_price'>%d</span>
                        <span class='font-black font-30'>грн</span>", "dverilending"),  intval(ThemeCustomizer::get_theme_setting("static_front_page_min_price", "1950"))) ?>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- block-1 -->

    <div id="block-2">
        <div class="container clearfix">
            <img src="/wp-content/themes/dverilending/img/logo-2.png">
            <?php show_main_menu(); ?>
            <div class="fl-right inl-block callback">
                <span class="recall_me_button" onclick="open_modal_call('menu')"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_recall_me", __("Перезвоните мне", "dverilending")) ?></span>
            </div>
            <div class="fl-right inl-block">
                <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone1-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></a><br>
                <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a>
            </div>
        </div>
    </div><!-- block-2 -->
    <div id="block-3">
        <div class="container clearfix">
            <h1 id="promotion_label"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_promotion_label", __('Акционные предложения', "dverilending")) ?></h1>
            <span class="prev-button"></span>
            <?php
            $is_first = true;
            $loop = new WP_Query(array(
                'post_type' => 'promotions',  // указываем, что выводить нужно именно товары
                'posts_per_page' => 100, // количество товаров для отображения
                'post_status' => 'publish', // получаем только опубликованые
                'orderby' => 'date', // тип сортировки (в данном случае по дате)
            ));
            $num = 0;
            while ($loop->have_posts()) :
                $loop->the_post();
            $unix_expiration = strtotime(get_post_meta( get_the_ID(), 'expiration', true )) - time();
            if($unix_expiration > 0) {
                ?>
                <div data-num="<?php echo $num;
                $num++ ?>" class='<?php echo !$is_first ? 'd-none ' : '' ?>act-div clearfix'>

                    <div class='act-div-left'>
                        <img class="lazy-load" src="<?php echo get_stylesheet_directory_uri() ?>/img/ajax-loader.gif" data-src="<?php the_post_thumbnail_url('original') ?>">
                    </div>

                    <div class='act-div-right'>
                        <span class='font-black font-24'><?php the_title() ?></span>
                        <?php echo str_replace("{regular_price}", get_post_meta(get_the_ID(), 'regular_price', true),
                            str_replace("{sale_price}", get_post_meta(get_the_ID(), 'sale_price', true),
                                str_replace("{difference_price}", get_post_meta(get_the_ID(), 'regular_price', true) - get_post_meta(get_the_ID(), 'sale_price', true), get_the_content())
                            )
                        ) ?>
                        <span class='font-light font-14 hidden-xs'><?php echo sprintf(__("акция действует до %s", "dverilending"), date('d.m', strtotime(get_post_meta(get_the_ID(), 'expiration', true)))) ?></span><br>
                        <span class='font-black font-14 hidden-xs'><?php echo sprintf(__("В наличии только %s дверей.", "dverilending"), get_post_meta(get_the_ID(), 'count', true)) ?></span>

                        <div class="timer">
                            <span class='font-light font-18'><?php esc_html_e("До конца акции осталось:", "dverilending") ?></span>
                            <ul>
                                <li>
                                    <?php esc_html_e("секунд", "dverilending") ?>
                                    <div class="tframe">
                                        <div class="tseconds">
                                            <span class="time-value"><?php echo date('s', $unix_expiration) ?></span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <?php esc_html_e("минут", "dverilending") ?>
                                    <div class="tframe">
                                        <div class="tmins">
                                            <span class="time-value"><?php echo date('i', $unix_expiration) ?></span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <?php esc_html_e("часов", "dverilending") ?>
                                    <div class="tframe">
                                        <div class="thours">
                                            <span class="time-value">
                                            <?php echo date('H', $unix_expiration) ?>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <?php esc_html_e("дней", "dverilending") ?>
                                    <div class="tframe">
                                        <div class="tdays"><span class="time-value"><?php echo date('d', $unix_expiration) ?></span></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix">
                            <div class="prices">
                                <span class='old-price'><?php echo sprintf("%s грн.", get_post_meta(get_the_ID(), 'regular_price', true)) ?></span><br>
                                <span class='act-price'><?php echo sprintf("%s<span class='font-regular'> грн.</span>", get_post_meta(get_the_ID(), 'sale_price', true)) ?></span>
                            </div>
                            <div class='act-info'><a style="color: white;"
                                                     href="<?php echo get_post_meta(get_the_ID(), 'link', true); ?>"><span
                                            class='font-black font-24'><?php esc_html_e("Подробнее", "dverilending") ?></span></a></div>
                        </div>
                    </div>
                </div>
                <?php
                $is_first = false;
            }
            endwhile;
            wp_reset_postdata();
            ?>
            <script type="text/javascript">
                var max_num_prom = <?php echo --$num ?>;
            </script>
            <span class="next-button"></span>
        </div>
    </div><!-- block-3 -->

    <div id=block-3a>
        <div class="container">
            <div class="small-act-imgs clearfix  hidden-xs">
                <?php
                $loop = new WP_Query(array(
                    'post_type' => 'promotions',  // указываем, что выводить нужно именно акции
                    'posts_per_page' => 100, // количество товаров для отображения
                    'post_status' => 'publish', // получаем только опубликованые
                    'orderby' => 'date', // тип сортировки (в данном случае по дате)
                ));
                $num = 0;
                while ($loop->have_posts()) :
                $loop->the_post();
                $unix_expiration = strtotime(get_post_meta( get_the_ID(), 'expiration', true )) - time();
                if($unix_expiration > 0) {
                    ?>
                    <img data-num="<?php echo $num;
                    $num++; ?>"  class="lazy-load" src="<?php echo get_stylesheet_directory_uri() ?>/img/ajax-loader.gif" data-src="<?php the_post_thumbnail_url('original') ?>">
                    <?php
                }
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div><!-- block-3a -->

    <div id="block-4">
        <div class="container">
            <h1 id="catalog_label"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_catalog_label", __('Каталог межкомнатных дверей', "dverilending")) ?></h1>
            <div class="cell-pseudo-table">
                <?php woocommerce_content(); ?>
            </div>
        </div>
    </div><!-- 4 block -->

    <div id="measurer-block">
        <div id="ruller_background" class="hidden-xs"></div>
        <div id="misurer" class="hidden-xs">
            <div class="container">
                <div class="col-sm-12" id="grace_the_measurer_title">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_grace_the_measurer_title", __('<div>Приграсите замерщика для замера двери.</div>
                    <div>Это бесплатно (даже, если вы не будете заказывать кухню у нас)</div>', "dverilending")) ?>
                </div>
                <div class="col-sm-6" id="grace_the_measurer_left_list">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_grace_the_measurer_left_list", __('<ul>
                        <li>Он правильно замерит размер кухни</li>
                        <li>Порекомендует дизайн со вкусом под стиль ремонта.</li>
                        <li>Почему важно выбрать цвет дома?</li>
                        <li>Расскажет, как выгодно использовать пространство</li>
                        <li>Подскажет, какой материа подойдёт под ваши нужды</li>
                    </ul>', "dverilending")) ?>
                </div>
                <div class="col-sm-6" id="grace_the_measurer_right_list">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_grace_the_measurer_right_list", __('<ul>
                        <li>Привезёт образцы фасадов, чтобы вы их прощупали и увидели вживую. Какие образцы дизайнер привезёт?</li>
                        <li>Приедет в удобное для вас время</li>
                        <li>Составит дизайн проект кухни</li>
                        <li>Оформит договор в случае заказа</li>
                    </ul>', "dverilending")) ?>
                </div>
            </div>
        </div>

        <div id="block-5">
        <div class="container">
            <div class="ruller-img"></div>
            <div class="men-quest"></div>
            <div class="buy-text">
                <div class="title" id="grace_the_measurer_block_title">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_grace_the_measurer_block_title", __('<span><nobr>входные двери</nobr></span>
                    <span><nobr>дешевле чем в</nobr></span>
                    <span>эпицентре</span>', "dverilending")) ?>
                </div>
                <div id="grace_the_measurer_block_text">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_grace_the_measurer_block_text", __('<span><nobr>Нашли дешевле?</nobr></span>
                    <span><nobr>продадим по вашей цене</nobr></span>', "dverilending")) ?>
                </div>
            </div>
            <div class="feedback-form">
                <?php echo do_shortcode('[contact-form-7 id="158"]') ?>
            </div>
        </div>
    </div><!-- block-5 -->
    </div>
    <div id="block-6">
        <div class="container clearfix">

            <h1 id="top_seven_title"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_top_seven_title", __('топ-7 самых надежных межкомнатных дверей', "dverilending")) ?></h1>
            <h3 id="top_seven_description"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_top_seven_description", __('по параметру цена-качество', "dverilending")) ?></h3>
            <span class="prev-button"></span>
            <?php
            $is_first = true;
            $loop = new WP_Query(array(
                'post_type' => 'topseven',  // указываем, что выводить нужно именно топ 7
                'posts_per_page' => 100, // количество товаров для отображения
                'orderby' => 'date', // тип сортировки (в данном случае по дате)
            ));
            $num = 0;
            while ($loop->have_posts()) :
            $loop->the_post();
            ?>
            <div class='<?php echo !$is_first ? 'd-none ' : '' ?>protect-div' data-num="<?php echo $num; $num++; ?>">
                <div class='protect-div-left'>
                    <img class="lazy-load" src="<?php echo get_stylesheet_directory_uri() ?>/img/ajax-loader.gif" data-src="<?php the_post_thumbnail_url('original') ?>">
                </div>

                <div class='protect-div-right'>
                    <h4><?php the_title() ?></h4>

                    <h5><?php esc_html_e("Дверная коробка:", "dverilending") ?></h5>
                    <span class="type"><?php esc_html_e("Тип рамы:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'tip_ramy', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Толщина рамы, мм:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'tolshina_ramy', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Толщина стали на коробке, мм:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'tolshina_stali_na_korobke', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Количество контуров уплотнения, шт:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'kolichestvo_konturov_uplotneniya', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Покраска металлической части:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'pokraska_metalicheskoy_chasti', true ); ?></span><br>

                    <h5><?php esc_html_e("Дверное полотно:", "dverilending") ?></h5>
                    <span class="type"><?php esc_html_e("Толщина полотна, мм:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'tolshina_polotna', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Толшина стали на полотне, мм:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'tolshina_stali_na_polotne', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Ребра жесткости, шт:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'rebra_zostkosti', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Металлический наличник:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'metalicheskiy_nilichnik', true ) == 1 ? '+' : '-'; ?></span><br>
                    <span class="type"><?php esc_html_e("Наполнение полотна:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'nopolnenie_polotna', true ); ?></span><br>

                    <h5><?php esc_html_e("Фурнитура двери:", "dverilending") ?></h5>
                    <span class="type"><?php esc_html_e("Тип ручки:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'tip_ruchki', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Нижний замок:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'nizniy_zamok', true ); ?></span><br>
                    <span class="type"><?php esc_html_e("Размер, мм:", "dverilending") ?></span>
                    <span class="answer"> <?php echo get_post_meta( get_the_ID(), 'total_size', true ); ?></span><br>

                </div>
            </div>
                <?php
                $is_first = false;
            endwhile;
            wp_reset_postdata();
            ?>
            <script type="text/javascript">
                var max_num_top = <?php echo --$num ?>;
            </script>
            <span class="next-button"></span>
        </div>
    </div> <!-- 6 block -->

    <div id="block-6a">
        <div class="container clearfix">
            <div class="small-prot-imgs">
                <?php
                $loop = new WP_Query(array(
                    'post_type' => 'topseven',  // указываем, что выводить нужно именно товары
                    'posts_per_page' => 100, // количество товаров для отображения
                    'orderby' => 'date', // тип сортировки (в данном случае по дате)
                ));
                $num = 0;
                while ($loop->have_posts()) :
                    $loop->the_post();
                    ?>
                    <img data-num="<?php echo $num; $num++; ?>" class="lazy-load" src="<?php echo get_stylesheet_directory_uri() ?>/img/ajax-loader.gif" data-src="<?php the_post_thumbnail_url('original') ?>">
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
            <?php
            $loop = new WP_Query(array(
            'post_type' => 'topseven',  // указываем, что выводить нужно именно товары
            'posts_per_page' => 100, // количество товаров для отображения
            'orderby' => 'date', // тип сортировки (в данном случае по дате)
            ));
            $num = 0;
            $is_first = true;
            while ($loop->have_posts()) :
            $loop->the_post();
            ?>
            <div data-num="<?php echo $num; ?>" class="<?php echo !$is_first ? 'd-none ' : '' ?>prices">
                <span class="old-price hidden-xs"><?php echo sprintf(__("%s грн.", "dverilending"), get_post_meta( get_the_ID(), 'regular_price', true )) ?></span>
                <span class="act-price"><?php echo sprintf(__("%s<span class='font-regular'> грн.</span>", "dverilending"), get_post_meta( get_the_ID(), 'sale_price', true )) ?></span>
            </div>
                <div data-num="<?php echo $num; $num++; ?>" class='<?php echo !$is_first ? 'd-none ' : '' ?>select-button'><a style="color: white;" href="<?php echo get_post_meta( get_the_ID(), 'link', true ); ?>"><span class='font-black font-24'><?php esc_html_e("Выбрать", "dverilending") ?></span></a></div>
            <?php
                $is_first = false;
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div><!-- 6-a -->

    <div id="block-7">
        <div class="container">
            <h1><?php esc_html_e("Выбор более 60 моделей", "dverilending") ?></h1>
            <div class="description" id="over_60_models_description">
                <?php echo ThemeCustomizer::get_theme_setting("static_front_page_over_60_models_description", __('<span>Приходите в наш салон по адресу:</span>
                <span>ул. Гетьмана 10/37</span>
                <span>сообщите менеджеру промокод:</span>
                <span>"двери дешево"</span>
                <span>и получите дополнительную скидку 5%</span>', "dverilending")) ?>
            </div>
            <a href="#block-13">
                <div class="show-on-map">
                    <span><?php esc_html_e("Показать на карте где мы находимся", "dverilending") ?></span>
                </div></a>
        </div>
    </div><!-- block 7-->

    <div id='block-8'>
        <div class="container">
            <div class="block-8-man"></div>
            <div class="block-8-text" id="installation_for_60_min_content">
                <?php echo ThemeCustomizer::get_theme_setting("static_front_page_installation_for_60_min_content", __('<span>Бесплатный</span>
                <span>монтаж за 60 мин</span>
                <span>мастерами с опытом от 10 лет</span>
                <div>
                    По статистике 80%  договечности работы входных дверей зависит от установки. А это очень важный шаг, потому что плохой монтаж от неопытного "мастера" ничего хорошего не сулит и могут образоваться щели между дверью и коробом, будет продувание, слабая шумоизоляция, а также плохо работать замочная система.
                </div>', "dverilending")) ?>
            </div>
        </div>
    </div><!-- block-8 -->

    <div id='block-9'>
        <div class="container">
            <div>
                <h2 id="delivery_first_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_delivery_first_line", __('доставка по Киеву', "dverilending")) ?></h2>
                <h2 id="delivery_second_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_delivery_second_line", __('за 120 минут', "dverilending")) ?></h2>
            </div>
        </div>
    </div><!-- block-9 -->

    <div id='block-10'>
        <div class="container">
            <div>
                <h1 id="about_company_title"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_title", __('О компании ″викоцентр″', "dverilending")) ?></h1>
                <div class="orders clearfix">
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-1.png" alt=""><span id="about_company_first_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_first_text", __('15 лет с 2001', "dverilending")) ?></span></div>
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-2.png" alt=""><span id="about_company_second_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_second_text", __('600 моделей дверей', "dverilending")) ?></span></div>
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-3.png" alt=""><span id="about_company_third_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_third_text", __('2590 дверей на складе', "dverilending")) ?></span></div>
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-4.png" alt=""><span id="about_company_fourth_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_fourth_text", __('Установлено 7268 дверей', "dverilending")) ?></span></div>
                </div>
                <h3 id="about_company_year"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_year", __('за 2015 год', "dverilending")) ?></h3>
                <h4 id="about_company_doors_count"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_doors_count", __('1163 двери куплено в нашей компании по Киеву', "dverilending")) ?></h4>
            </div>
        </div>
    </div><!-- block-10 -->

    <div id='block-11'>
        <div class="container">
            <div>
                <h1><?php esc_html_e("Отзывы. Что говорят клиенты!", "dverilending") ?></h1>
                <div class="clients-comments clearfix">

                    <div id="myCarousel" class="carousel slide" data-interval="false" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php
                            $is_first = true;
                            $loop = new WP_Query(array(
                                'post_type' => 'otzyv',  // указываем, что выводить нужно именно отзывы
                                'posts_per_page' => 100, // количество товаров для отображения
                                'post_status' => 'publish', // получаем только опубликованые
                                'orderby' => 'date', // тип сортировки (в данном случае по дате)
                            ));
                            $num = 0;
                            while ($loop->have_posts()) :
                            $loop->the_post();
                            if($num == 0)
                            {
                            ?>
                            <div class="item<?php echo $is_first ? ' active' : '' ?>">
                                <?php
                                $is_first = false;
                            } ?>
                                <div class="client-comment">
                                    <div class="client-photo"><img src="<?php the_post_thumbnail_url('original') ?>"></div>
                                    <div class="client-text"><span class="qts"></span>
                                        <?php the_content() ?>
                                    </div>
                                </div>
                            <?php
                            $num++;
                            if($num == 2) {
                                ?>
                            </div>
                                <?php
                                $num = 0;
                            }
                            endwhile;
                            wp_reset_postdata();
                            ?>

                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="prev-button"></span>
                            <span class="sr-only"><?php esc_html_e("Предыдущий", "dverilending") ?></span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="next-button"></span>
                            <span class="sr-only"><?php esc_html_e("Следующий", "dverilending") ?></span>
                        </a>
                    </div>

                    <div id="myCarouselMobile" class="carousel slide" data-interval="false" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php
                            $is_first = true;
                            $loop = new WP_Query(array(
                                'post_type' => 'otzyv',  // указываем, что выводить нужно именно отзывы
                                'posts_per_page' => 100, // количество товаров для отображения
                                'post_status' => 'publish', // получаем только опубликованые
                                'orderby' => 'date', // тип сортировки (в данном случае по дате)
                            ));
                            $num = 0;
                            while ($loop->have_posts()) :
                                $loop->the_post();?>
                                    <div class="item<?php echo $is_first ? ' active' : '' ?>">
                                <div class="client-comment">
                                    <div class="client-photo"><img src="<?php the_post_thumbnail_url('original') ?>"></div>
                                    <div class="client-text"><span class="qts"></span>
                                        <?php the_content() ?>
                                    </div>
                                </div>
                                    </div>
                                    <?php
                                $is_first = false;
                            endwhile;
                            wp_reset_postdata();
                            ?>

                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarouselMobile" data-slide="prev">
                            <span class="prev-button"></span>
                            <span class="sr-only"><?php esc_html_e("Предыдущий", "dverilending") ?></span>
                        </a>
                        <a class="right carousel-control" href="#myCarouselMobile" data-slide="next">
                            <span class="next-button"></span>
                            <span class="sr-only"><?php esc_html_e("Следующий", "dverilending") ?></span>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- block-11 -->

    <div id='block-12'>
        <div class="container">
            <div class="manager-bg"></div>
            <div class='block-12-text'>
                <div>
                    <?php _e('<h2>Есть вопрос?</h2>
                    <h2>Не можете определиться?</h2>
                    <div class="let-number">ОСТАВЬТЕ СВОЙ НОМЕР ТЕЛЕФОНА НАШ СПЕЦИАЛИСТ СВЯЖЕТСЯ С ВАМИ ЧЕРЕЗ 25 СЕКУНД И ПОМОЖЕТ С ВЫБОРОМ И КОНСУЛЬТАЦИЕЙ</div>', "dverilending"); ?>
                    <?php echo do_shortcode('[contact-form-7 id="157"]') ?>
                </div>
            </div>
        </div>
    </div><!-- block-12 -->

    <div id='block-13'>
        <div class="g-map">
            <div class="g-info">
                <div class="info">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_google_map_info", __('Приходите в наш салон по адресу:<br> ул. Гетьмана 10/37<br> сообщите менеджеру промокод:<br> "двери дешево"<br> и получите дополнительную скидку 5%', "dverilending")) ?>
                </div>
                <div class="phones">
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone1-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></a>
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone2-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a>
                </div>
                <a href="<?php echo ThemeCustomizer::get_theme_setting("static_front_page_google_map_link", __('https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%92%D0%B0%D0%B4%D0%B8%D0%BC%D0%B0+%D0%93%D0%B5%D1%82%D1%8C%D0%BC%D0%B0%D0%BD%D0%B0,+10,+%D0%9A%D0%B8%D0%B5%D0%B2', "dverilending")) ?>" target="_blank" class="show-on-map" id="google_map_link">
                    <span><?php esc_html_e("Показать на карте где мы находимся", "dverilending"); ?></span></a>
            </div>
        </div>
    </div><!-- block-13 -->

    <div id='block-14'>
        <div class="container clearfix">
            <div class="footer-about">
                <div class="ftr-logo">
                    <img src="/wp-content/themes/dverilending/img/logo.png" alt="">
                </div>
                <div class="copyright">
                    &#169; <?php echo !empty($theme_options['copyright_start_year']) && is_numeric($theme_options['copyright_start_year']) ? $theme_options['copyright_start_year'].'-'.date('Y') : date('Y') ?> <?php bloginfo('name') ?>. <?php esc_html_e("Все права защищены", "dverilending")?>
                </div>
                <div class="vk-btn-link">
                    <a href="<?php echo !empty($theme_options['link_fb']) ? $theme_options['link_fb'] : '' ?>"><img src="/wp-content/themes/dverilending/img/ik-fb.png" alt=""></a>
                    <a class="margin-left-10px" href="<?php echo !empty($theme_options['link_ins']) ? $theme_options['link_ins'] : '' ?>"><img src="/wp-content/themes/dverilending/img/ik-ins.png" alt=""></a>
                </div>
            </div>
            <div class='recommended-links' id="recomended_links">
                <?php echo ThemeCustomizer::get_theme_setting("static_front_page_recomended_links", __('<div>Также рекомендуем посетить:</div>
                <a href="http://dverivh.com.ua">dverivh.com.ua</a>
                <a href="http://okna.com.ua">okna.com.ua</a>
                <a href="http://sma.com.ua">sma.com.ua</a>', "dverilending")) ?>
            </div>
            <div class='bye-and-contacts'>
                <a href="#" onclick="open_modal_call('footer')" class="ftr-bye-door-btn"><?php esc_html_e("Купить двери", "dverilending") ?></a>
                <div class="ftr-contact-phones clearfix">
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone1-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></a>
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone2-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a>
                </div>
                <div class="ftr-addr font-light font-14" id="footer_address">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_footer_address", __('<div>ул. Гетьмана 10/37</div>
                    <div>с 9:00 до 19:00 без выходных</div>', "dverilending")) ?>
                </div>

            </div>
        </div>
    </div><!-- block-14 - footer -->

</div>

<!-- Modal -->
<div class="modal fade" id="callWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center">
                    <h5 class="font-24 font-bold margin-top-10px margin-bottom-10px"><?php esc_html_e("Обратная связь", "dverilending") ?></h5>
                    <?php
                    echo do_shortcode('[contact-form-7 id="159"]');
                    ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="call_success_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body text-center">
                <div id="call_success">
                    <div class="w-48p ml-10px"><img src="/wp-content/themes/dverilending/img/success.png"></div>
                    <div class="w-48p">
                        <h3 class="font-bold font-30 margin-top-10px"><?php esc_html_e("Спасибо что заказали двери у нас", "dverilending") ?></h3>
                        <div class="margin-top-10px">
                            <?php esc_html_e("В течении 25 секунд наш менеджер с Вами свяжется и уже через час дверь будет установлена!", "dverilending") ?>
                        </div>
                        <div class="margin-top-10px">
                            <?php esc_html_e("Так же Вы можете связаться с нами по номеру:", "dverilending") ?>
                        </div>
                        <div class="margin-top-10px">
                            <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="phone1-link"<?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></span></a>
                        </div>
                        <div class="margin-top-10px">
                            <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a>
                        </div>
                        <div class="margin-top-50px font-18 font-bold"><?php esc_html_e("Так же Вы можете ознакомиться с другой нашей продукцией", "dverilending"); ?></div>
                        <div class="margin-top-10px others">
                            <div class="w-20p"><img src="/wp-content/themes/dverilending/img/zabor.png"></div>
                            <div class="w-70p margin-left-10px" id="zabotri"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_zabotri", __('Изготовление и установка заборов<br><a href="http://zabotri.com.ua">zabotri.com.ua</a>', "dverilending")) ?></div>
                        </div>
                        <div class="margin-top-10px others">
                            <div class="w-20p"><img src="/wp-content/themes/dverilending/img/balkon.png"></div>
                            <div class="w-70p margin-left-10px" id="balckon"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_balckon", __('Застекливание и утепление балконов<br><a href="http://balkoni.com.ua">balkoni.com.ua</a>', "dverilending")) ?></div>
                        </div>
                        <div class="margin-top-10px others">
                            <div class="w-20p"><img src="/wp-content/themes/dverilending/img/okno.png"></div>
                            <div class="w-70p margin-left-10px" id="okna"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_okna", __('Изготовление и установка окон<br><a href="http://okna.com.ua">okna.com.ua</a>', "dverilending")) ?></div>
                        </div>
                        <div class="margin-top-10px others">
                            <div class="w-20p"><img src="/wp-content/themes/dverilending/img/dveri.png"></div>
                            <div class="w-70p margin-left-10px" id="doors"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_doors", __('Изготовление и установка межкомнатных дверей<br><a href="http://dveri.com.ua">dveri.com.ua</a>', "dverilending")) ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
<script type="text/javascript">
    function open_modal_call(place)
    {
        switch (place)
        {
            case 'head':
                jQuery('#callWindow [name=place]').val('Шапка. Напротив логотипа');
                break;
            case 'menu':
                jQuery('#callWindow [name=place]').val('Кнопка в меню');
                break;
            case 'product':
                jQuery('#callWindow [name=place]').val('Страница товара возле кнопки купить в первом блоке');
                break;
            case 'footer':
                jQuery('#callWindow [name=place]').val('Купить двери футер, главная');
                break;
        }
        jQuery('#callWindow').modal('show');
        return false;
    }
    jQuery(function($) {
        $(document).ready(function () {
            $('body').on('click', 'a.load-more', function () {
                var fmdata = $('form#filters').serializeArray();
                var formdata = new FormData();
                var num = 0;
                fmdata.forEach(function(item, index, d){
                    var res = fmdata.filter(function(i) {
                        return i.name == item.name;
                    });
                    if(res.length > 1)
                    {
                        formdata.append('filters[' + item.name + '][' + num + ']', item.value);
                        num++;
                    }
                    else
                    {
                        num = 0;
                        formdata.append('filters[' + item.name + ']', item.value);
                    }
                    if(res.length == num + 1)
                    {
                        num = 0;
                    }
                });
                formdata.append('action', 'loadmore');
                formdata.append('page', $(this).data('page').toString());
                var more_button = this;
                $.ajax({
                    url: woocommerce_params.ajax_url,
                    data: formdata,
                    type: 'POST',
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    beforeSend: function (xhr) {
                        $('body').addClass('loading');
                    },
                    success: function (data) {
                        if (data) {
                            $('div.cell-pseudo-table div.row.good').append(data.html);
                            $('body').removeClass('loading');
                            var next_page = $(more_button).data('page') + 1;
                            console.log(next_page);
                            $(more_button).data('page', next_page);
                            if(data.max_page < next_page)
                            {
                                $(more_button).parent('div.other-models-greeting').remove();
                            }
                        }
                    }
                });
                return false;
            });



            $('.wpcf7').on('wpcf7mailsent', function () {
                $('#callWindow').modal('hide');
                $('#call_success_modal').modal('show');
            });

            var old_margin_top = $('#block-3').css('margin-top');
            old_margin_top = parseInt(old_margin_top.replace(/\D+/g,""));

            var margin_top = $('#block-2').height() + old_margin_top;
            if($(window).width() <= 765)
            {
                margin_top = $('.mobile #block-1 .top-head').height() + old_margin_top;
            }

            function sticky() {
                var y = $(window).scrollTop();
                var top = '0';

                if($('#wpadminbar').length > 0 && $(window).width() > 766)
                {
                    top = $('#wpadminbar').height();
                }

                if( y > $('#block-1').height() + $('#wpadminbar').height() + 12 ){
                    $('#block-2').css({
                        'position': 'fixed',
                        'top': top,
                        'width': $('#block-1').width(),
                        'background': '#fff',
                        'z-index': '9000',
                    });
                    $('#block-3').css('margin-top', margin_top + 'px');
                } else {
                    $('#block-2').removeAttr('style');
                    $('#block-3').css('margin-top', old_margin_top + 'px');
                }

                if($(window).width() <= 765) {
                    if (y > $('.mobile #block-1 .top-head').height()) {
                        $('.mobile #block-1 .top-head').css({
                            'position': 'fixed',
                            'top': top,
                            'width': $('#block-1').width(),
                            'background': '#fff',
                            'z-index': '9000',
                        });
                        $('.mobile #block-1 .right').css('margin-top', margin_top + 16 + 'px');
                    } else {
                        $('.mobile #block-1 .top-head').removeAttr('style');
                        $('.mobile #block-1 .right').css('margin-top', '5px');
                    }
                }
            }
            $('.good-filter-form .static-filter').change(function () {
                $(this).closest('form').submit();
            });

            $('.good-filter-form').submit(function () {
                var fmdata = $(this).serializeArray();
                var formdata = new FormData();
                $('.cell-pseudo-table .modal').modal('hide');
                var num = 0;
                fmdata.forEach(function(item, index, d){
                    if(item.name != 'price_range')
                    {
                        formdata.append('filters[' + item.name + '][' + num + ']', item.value);
                        num++;
                    }
                    else
                    {
                        num = 0;
                        formdata.append('filters[' + item.name + ']', item.value);
                    }
                });
                formdata.append('action', 'loadmore');
                formdata.append('page', 1);
                var table = $(this).closest('.cell-pseudo-table');
                var more_button = $(table).children('.other-models-greeting.text-center');
                $.ajax({
                    url: woocommerce_params.ajax_url,
                    data: formdata,
                    type: 'POST',
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    beforeSend: function (xhr) {
                        $('body').addClass('loading');
                    },
                    success: function (data) {
                        if (data) {
                            $('div.cell-pseudo-table div.row.good').html(data.html);
                            $('body').removeClass('loading');
                            if(more_button.length > 0)
                            {
                                $(more_button).data('page', 2);
                            }
                            else
                            {
                                $(table).append('<div class="other-models-greeting text-center"><a class="text-white load-more" data-page="2" href="javascript: void (0)"><span>Другие модели</span></a></div>');
                            }
                            if(data.html == '')
                            {
                                $('div.cell-pseudo-table div.row.good').html('<div class="w-100p">Ни одного элемента не найдено</div>');
                            }
                            $('body').removeClass('ajax-loader');
                        }
                    }
                });
                return false;
            });

            $('.multiselect-ui').multiselect({
                includeSelectAllOption: true
            });

            sticky();
            $(window).scroll(sticky);
            $(window).resize(sticky);

            $('#callWindow input, #block-5 input, #block-12 input').focus(function () {
                var placeholder = $(this).attr('placeholder');
                $(this).attr('placeholder', '');
                $('#callWindow input, #block-5 input, #block-12 input').blur(function () {
                    $(this).attr('placeholder', placeholder);
                    $(this).off('blur');
                });
            });

            var promotion_current_slide = 0;
            var top_current_slide = 0;

            $('#block-3 span.prev-button').click(function () {
                if(promotion_current_slide > 0)
                {
                    $('#block-3 div.act-div.clearfix[data-num=' + promotion_current_slide + ']').addClass('d-none');
                    promotion_current_slide--;
                    $('#block-3 div.act-div.clearfix[data-num=' + promotion_current_slide + ']').removeClass('d-none');
                }
            });

            $('#block-3 span.next-button').click(function () {
                if(promotion_current_slide < max_num_prom)
                {
                    $('#block-3 div.act-div.clearfix[data-num=' + promotion_current_slide + ']').addClass('d-none');
                    promotion_current_slide++;
                    $('#block-3 div.act-div.clearfix[data-num=' + promotion_current_slide + ']').removeClass('d-none');
                }
            });

            $('#block-3a div.small-act-imgs.clearfix img').click(function () {
                $('#block-3 div.act-div.clearfix[data-num=' + promotion_current_slide + ']').addClass('d-none');
                promotion_current_slide = $(this).data('num');
                $('#block-3 div.act-div.clearfix[data-num=' + promotion_current_slide + ']').removeClass('d-none');
            });

            $('#block-6 span.prev-button').click(function () {
                if(top_current_slide > 0)
                {
                    $('#block-6 div.protect-div[data-num=' + top_current_slide + ']').addClass('d-none');
                    $('#block-6a div.prices[data-num=' + top_current_slide + ']').addClass('d-none');
                    $('#block-6a div.select-button[data-num=' + top_current_slide + ']').addClass('d-none');
                    top_current_slide--;
                    $('#block-6 div.protect-div[data-num=' + top_current_slide + ']').removeClass('d-none');
                    $('#block-6a div.prices[data-num=' + top_current_slide + ']').removeClass('d-none');
                    $('#block-6a div.select-button[data-num=' + top_current_slide + ']').removeClass('d-none');
                }
            });

            $('#block-3 div.timer').each(promTimer);
            function promTimer(index, elem)
            {
                var $timer_cont = $(elem);
                var $timer_seconds_progress_el = $(this).find('div.tseconds')[0];
                var $timer_seconds_value_el = $($timer_seconds_progress_el).find('span.time-value')[0];
                var $timer_minutes_progress_el = $(this).find('div.tmins')[0];
                var $timer_minutes_value_el = $($timer_minutes_progress_el).find('span.time-value')[0];
                var $timer_hours_progress_el = $(this).find('div.thours')[0];
                var $timer_hours_value_el = $($timer_hours_progress_el).find('span.time-value')[0];
                var $timer_days_progress_el = $(this).find('div.tdays')[0];
                var $timer_days_value_el = $($timer_days_progress_el).find('span.time-value')[0];
                if($timer_days_progress_el !== undefined && $timer_hours_progress_el !== undefined && $timer_minutes_progress_el !== undefined && $timer_seconds_progress_el !== undefined) {
                    var days = $($timer_days_value_el).html();
                    var hours = $($timer_hours_value_el).html();
                    var minutes = $($timer_minutes_value_el).html();
                    var seconds = $($timer_seconds_value_el).html();
                    if(seconds == 0 && (minutes != 0 || hours != 0 || days != 0))
                    {
                        seconds = 60;
                    }
                    if(minutes == 0 && (hours != 0 || days != 0))
                    {
                        minutes = 59;
                    }
                    if(hours == 0 && days != 0)
                    {
                        hours = 23;
                    }
                    var max_days_miliseconds = 15552000000;
                    var hours_miliseconds = hours * 3600000;
                    var minutes_miliseconds = minutes * 60000;
                    var seconds_miliseconds = seconds * 1000;
                    var days_miliseconds_all = days * 86400000 + hours_miliseconds + minutes_miliseconds + seconds_miliseconds;
                    var current_progress = days_miliseconds_all / max_days_miliseconds;
                    $($timer_days_progress_el).circleProgress({
                        value: 0.0,
                        size: 54,
                        fill: '#ef892a',
                        emptyFill: 'rgba(0, 0, 0, 0.0)',
                        animation: {duration: days_miliseconds_all, easing: "linear"},
                        reverse: true,
                        animationStartValue: current_progress
                    });

                    $($timer_days_progress_el).on('circle-animation-progress', function( event, animationProgress, stepValue ) {
                        $($timer_days_value_el).html(parseInt(180 * stepValue));
                    });

                    var hours_miliseconds_all = hours_miliseconds + minutes_miliseconds + seconds_miliseconds;
                    var current_progress = hours_miliseconds_all / (3600000 * 24);
                    $($timer_hours_progress_el).circleProgress({
                        value: 0.0,
                        size: 54,
                        fill: '#ef892a',
                        emptyFill: 'rgba(0, 0, 0, 0.0)',
                        animation: {duration: hours_miliseconds_all, easing: "linear"},
                        reverse: true,
                        animationStartValue: current_progress
                    });

                    $($timer_hours_progress_el).on('circle-animation-progress', function( event, animationProgress, stepValue ) {
                        $($timer_hours_value_el).html(parseInt(24 * stepValue));
                        if($($timer_hours_value_el).html() == '0' && $($timer_days_value_el).html() != 0)
                        {
                            $($timer_hours_value_el).circleProgress({
                                value: 0.0,
                                size: 54,
                                fill: '#ef892a',
                                emptyFill: 'rgba(0, 0, 0, 0.0)',
                                animation: {duration: 86400000, easing: "linear"},
                                reverse: true,
                                animationStartValue: 1.0
                            });
                        }
                    });

                    var minutes_miliseconds_all = minutes_miliseconds + seconds_miliseconds;
                    var current_progress = minutes_miliseconds_all / (60000 * 60);
                    $($timer_minutes_progress_el).circleProgress({
                        value: 0.0,
                        size: 54,
                        fill: '#ef892a',
                        emptyFill: 'rgba(0, 0, 0, 0.0)',
                        animation: {duration: minutes_miliseconds_all, easing: "linear"},
                        reverse: true,
                        animationStartValue: current_progress
                    });

                    $($timer_minutes_progress_el).on('circle-animation-progress', function( event, animationProgress, stepValue ) {
                        $($timer_minutes_value_el).html(parseInt(60 * stepValue));
                        if($($timer_minutes_value_el).html() == '0' && ( $($timer_hours_value_el).html() != 0 || $($timer_days_value_el).html() != 0 ))
                        {
                            $($timer_minutes_progress_el).circleProgress({
                                value: 0.0,
                                size: 54,
                                fill: '#ef892a',
                                emptyFill: 'rgba(0, 0, 0, 0.0)',
                                animation: {duration: 3600000, easing: "linear"},
                                reverse: true,
                                animationStartValue: 1.0
                            });
                        }
                    });

                    var current_progress = seconds_miliseconds / (1000 * 60);
                    $($timer_seconds_progress_el).circleProgress({
                        value: 0.0,
                        size: 54,
                        fill: '#ef892a',
                        emptyFill: 'rgba(0, 0, 0, 0.0)',
                        animation: {duration: seconds_miliseconds, easing: "linear"},
                        reverse: true,
                        animationStartValue: current_progress
                    });

                    $($timer_seconds_progress_el).on('circle-animation-progress', function( event, animationProgress, stepValue ) {
                        $($timer_seconds_value_el).html(parseInt(60 * stepValue));
                        if($($timer_seconds_value_el).html() == '0' && ( $($timer_minutes_value_el).html() != 0 || $($timer_hours_value_el).html() != 0 || $($timer_days_value_el).html() != 0 ))
                        {
                            $($timer_seconds_progress_el).circleProgress({
                                value: 0.0,
                                size: 54,
                                fill: '#ef892a',
                                emptyFill: 'rgba(0, 0, 0, 0.0)',
                                animation: {duration: 60000, easing: "linear"},
                                reverse: true,
                                animationStartValue: 1.0
                            });
                        }
                    });
                }
            }

            $('#block-6 span.next-button').click(function () {
                if(top_current_slide < max_num_top)
                {
                    console.log('work!');
                    $('#block-6 div.protect-div[data-num=' + top_current_slide + ']').addClass('d-none');
                    $('#block-6a div.prices[data-num=' + top_current_slide + ']').addClass('d-none');
                    $('#block-6a div.select-button[data-num=' + top_current_slide + ']').addClass('d-none');
                    top_current_slide++;
                    $('#block-6 div.protect-div[data-num=' + top_current_slide + ']').removeClass('d-none');
                    $('#block-6a div.prices[data-num=' + top_current_slide + ']').removeClass('d-none');
                    $('#block-6a div.select-button[data-num=' + top_current_slide + ']').removeClass('d-none');
                }
            });

            $('#block-6a div.small-prot-imgs img').click(function () {
                $('#block-6 div.protect-div[data-num=' + top_current_slide + ']').addClass('d-none');
                $('#block-6a div.prices[data-num=' + top_current_slide + ']').addClass('d-none');
                $('#block-6a div.select-button[data-num=' + top_current_slide + ']').addClass('d-none');
                top_current_slide = $(this).data('num');
                $('#block-6 div.protect-div[data-num=' + top_current_slide + ']').removeClass('d-none');
                $('#block-6a div.prices[data-num=' + top_current_slide + ']').removeClass('d-none');
                $('#block-6a div.select-button[data-num=' + top_current_slide + ']').removeClass('d-none');
            });

            $('body').on('click', 'div.cell-pseudo-table .cell a', function()
            {
                var selected_filters = {};
                var selId = '';
                $('form#filters select option:selected').each(function(i,elem) {
                    selId = $(this).closest('select').attr('id');
                    if(!(selId in selected_filters)) {
                        selected_filters[selId] = {};
                    }
                    selected_filters[selId][Object.keys(selected_filters[selId]).length] = $(this).attr('value');
                });
                localStorage.setItem("price_range", JSON.stringify(price_filter.getValue()));
                localStorage.setItem("good_filters", JSON.stringify(selected_filters));
                localStorage.setItem("MainScroolTop", $(document).scrollTop());
            });

            if(localStorage.getItem('price_range') !== null && localStorage.getItem('MainScroolTop') !== null)
            {
                var scrollTop = localStorage.getItem('MainScroolTop');
                var filters = JSON.parse(localStorage.getItem('good_filters'));
                var price_range = JSON.parse(localStorage.getItem('price_range'));
                price_filter.setValue(price_range);
                for(var selectID in filters)
                {
                    for(var optionVal in filters[selectID])
                    {
                        $('select#' + selectID + ' option[value="' + filters[selectID][optionVal] + '"]').attr('selected', 'selected');
                    }
                }
                $('.multiselect-ui').multiselect('rebuild');
                $(document).scrollTop(scrollTop);
                localStorage.removeItem('MainScroolTop');
                localStorage.removeItem('good_filters');
                localStorage.removeItem('price_range');
                $('form#filters').submit();
            }
        });
    });
</script>

<div class="windows8">
    <div class="wBall" id="wBall_1">
        <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_2">
        <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_3">
        <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_4">
        <div class="wInnerBall"></div>
    </div>
    <div class="wBall" id="wBall_5">
        <div class="wInnerBall"></div>
    </div>
</div>
<div class="ajax-loader-background"></div>

</body>
</html>
