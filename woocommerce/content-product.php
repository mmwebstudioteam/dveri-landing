<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="col-xs-12 col-sm-4 col-lg-3 position-relative">
    <div class="cell">
        <div class="img-wrapper">
            <div class="small-door"><a href="<?php the_permalink() ?>"><?php do_action( 'dveri_product_tumbnails' ); ?></a></div>
            <?php
            if($product->is_on_sale())
            {
                ?>
                <div class="actions"><?php esc_html_e("Акция!", "dverilending") ?></div>
            <?php
            }
            ?>
        </div>
        <div class="descr-door">
            <a href="<?php the_permalink() ?>"><?php do_action('woocommerce_shop_loop_item_title') ?></a>
            <div>
                <span><?php the_excerpt() ?></span>
            </div>
            <a href="<?php the_permalink() ?>" class="details-button">
                <?php esc_html_e("Подробнее", "dverilending") ?>
            </a>
            <?php
            $price = get_post_meta( get_the_ID(), '_regular_price', true);
            $sale = get_post_meta( get_the_ID(), '_price', true);
            if($product->is_on_sale())
            {
            ?>
                <span class="old-price"><?php echo sprintf(__("%s грн.", "dverilending"), $price) ?></span><br>
                <span class="new-price"><?php echo sprintf(__("%s грн.", "dverilending"), $sale) ?></span>
            <?php }
            else
            {
                    ?>
                <span class="price"><?php echo sprintf(__("%s грн.", "dverilending"), $price) ?></span>
            <?php
            }
            ?>
        </div>
    </div>
</div>
