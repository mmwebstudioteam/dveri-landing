<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
global $product;
global $device_type;
$attachment_ids = $product->get_gallery_attachment_ids();
$fields = get_field_objects(get_the_ID());$fields_sort=array();
if( $fields ) {
    foreach( $fields as $field_name => $field ) {
        if ( (int)$field['parent']==110 && !empty($field['value']) )  {
            if (is_array($field['value'])) {
                $field_value = implode(", ", $field['value']);
            } else {
                $field_value = $field['value'];
            }
            $fields_sort['door_box'][] = array(
                    'label' => $field['label'],
                    'value' => $field_value
            );
        }
        else if((int)$field['parent']==116 && !empty($field['value']))
        {
            if (is_array($field['value'])) {
                $field_value = implode(", ", $field['value']);
            } else {
                $field_value = $field['value'];
            }
            $fields_sort['door_pannel'][] = array(
                'label' => $field['label'],
                'value' => $field_value
            );
        }
        else if((int)$field['parent']==122 && !empty($field['value']))
        {
            if (is_array($field['value'])) {
                $field_value = implode(", ", $field['value']);
            } else {
                $field_value = $field['value'];
            }
            $fields_sort['door_forniture'][] = array(
                'label' => $field['label'],
                'value' => $field_value
            );
        }
    }
}
?>
<?php
if(array_key_exists('submit_order', $_POST) )
{
    $client_name = sanitize_text_field( $_POST['сname'] );
    $client_phone = sanitize_text_field( $_POST['phone'] );
    $client_address = sanitize_text_field( $_POST['address'] );
    $address = array(
        'first_name' => sanitize_text_field( $_POST['сname'] ),
        'last_name'  => '-',
        'company'    => '-',
        'email'      => 'no-email@noemail.loc',
        'phone'      => sanitize_text_field( $_POST['phone'] ),
        'address_1'  => sanitize_text_field( $_POST['address'] ),
        'address_2'  => '-',
        'city'       => '-',
        'state'      => 'ZP',
        'postcode'   => '00000',
        'country'    => 'UA'
    );
    $order = wc_create_order();
    $order->add_product( $product, 1 ); //(get_product with id and next is for quantity)
    $order->set_address( $address, 'billing' );
    $order->set_address( $address, 'shipping' );
    $order->set_status('processing');
    $order->calculate_totals();
    $theme_options  = get_option('true_options');
    $price_caption = "Стоимость: ".get_post_meta(get_the_ID(), '_regular_price', true)." грн.";
    if(get_post_meta( get_the_ID(), '_price', true) != get_post_meta(get_the_ID(), '_regular_price', true))
        {
            $price_caption .= "\nАкционная стоимость(цена продажи): ".get_post_meta(get_the_ID(), '_price', true)." грн.";
        }
    $message = "Создан новый заказ №".$order->get_id()."
Имя клиента: ".$client_name."
Номер телефона клиента: ".$client_phone."
Адресс доставки: ".$client_address."
Наименование товара: ".get_the_title()."
".$price_caption."
Дата заказа: ".date("d.m.Y H:i:s");
    $message = urlencode($message);
    file_get_contents('https://api.telegram.org/bot'.$theme_options['bot_token'].'/sendMessage?chat_id='.$theme_options['chat_id'].'&text='.$message);
    $message = urldecode($message);
    wp_mail($theme_options['notify_email'], "Новый заказ №".$order->get_id(), $message);
    ?>
    <div id="buy_success">
        <div class="w-48p ml-10px"><img src="/wp-content/themes/dverilending/img/success.png"></div>
        <div class="w-48p">
            <h3 class="font-bold font-30 margin-top-10px"><?php esc_html_e("Спасибо что заказали двери у нас", "dverilending") ?></h3>
            <div class="margin-top-10px">
                <?php esc_html_e("В течении 25 секунд наш менеджер с Вами свяжется и уже через час дверь будет установлена!", "dverilending") ?>
            </div>
            <div class="margin-top-10px">
                <?php esc_html_e("Так же Вы можете связаться с нами по номеру:", "dverilending") ?>
            </div>
            <div class="margin-top-10px">
                <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="phone1-link"<?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></span></a>
            </div>
            <div class="margin-top-10px">
                <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a>
            </div>
            <div class="margin-top-50px font-18 font-bold"><?php esc_html_e("Так же Вы можете ознакомиться с другой нашей продукцией", "dverilending"); ?></div>
            <div class="margin-top-10px others">
                <div class="w-20p"><img src="/wp-content/themes/dverilending/img/zabor.png"></div>
                <div class="w-70p margin-left-10px" id="zabotri"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_zabotri", __('Изготовление и установка заборов<br><a href="http://zabotri.com.ua">zabotri.com.ua</a>', "dverilending")) ?></div>
            </div>
            <div class="margin-top-10px others">
                <div class="w-20p"><img src="/wp-content/themes/dverilending/img/balkon.png"></div>
                <div class="w-70p margin-left-10px" id="balckon"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_balckon", __('Застекливание и утепление балконов<br><a href="http://balkoni.com.ua">balkoni.com.ua</a>', "dverilending")) ?></div>
            </div>
            <div class="margin-top-10px others">
                <div class="w-20p"><img src="/wp-content/themes/dverilending/img/okno.png"></div>
                <div class="w-70p margin-left-10px" id="okna"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_okna", __('Изготовление и установка окон<br><a href="http://okna.com.ua">okna.com.ua</a>', "dverilending")) ?></div>
            </div>
            <div class="margin-top-10px others">
                <div class="w-20p"><img src="/wp-content/themes/dverilending/img/dveri.png"></div>
                <div class="w-70p margin-left-10px" id="doors"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_doors", __('Изготовление и установка межкомнатных дверей<br><a href="http://dveri.com.ua">dveri.com.ua</a>', "dverilending")) ?></div>
            </div>
        </div>
    </div>
    <?php
}
else
{
    $device = new Mobile_Detect();
    ?>
<div id="product-<?php the_ID(); ?>">
    <div id = "product_main_menu">
        <div class="container clearfix">
            <div class="float-left">
                <div class="back-to-catalog">
                    <a href="/"><?php esc_html_e("&#9668; НАЗАД ", "dverilending") ?></a>
                </div>
                <h1 class="pceudotitle"><?php the_title() ?></h1>
            </div>
            <div class="float-right hidden-xs">
                <?php if ( $product->get_sku() ) { ?>
                <div class="articule d-inline-block"><?php esc_html_e("Артикул ", "dverilending") ?><?php echo $product->get_sku() ?></div>
                <?php } ?>
                <div class="price d-inline-block">
                    <?php
                    if(get_post_meta( get_the_ID(), '_price', true) != get_post_meta(get_the_ID(), '_regular_price', true))
                    {
                        ?>
                        <?php if($device_type != 'mobile') { echo sprintf(__('<span class="old-price">%s</span> грн.', "dverilending"), get_post_meta(get_the_ID(), '_regular_price', true)); } ?>
                        <span class="act-price font-18 font-bold"><?php echo sprintf(__('%s<span class="font-regular"> грн.</span>', "dverilending"), get_post_meta( get_the_ID(), '_price', true)) ?></span>
                        <?php
                    }
                    else
                    {
                    ?>
                        <span class="act-price"><?php echo sprintf(__('%s<span class="font-regular"> грн.', "dverilending"), get_post_meta( get_the_ID(), '_regular_price', true))?></></span>
                    <?php } ?>
                </div>
                <div class="buy-button d-inline-block">
                    <a data-toggle="modal" href="#buyWindow" style="color: white;">
                        <span class="font-black font-18"><?php esc_html_e("Купить", "dverilending") ?></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div id="block-3">
        <div class="container clearfix">
            <span class="prev-button good"></span>
            <div class="act-div clearfix">

                <div class="act-div-left">
                    <img data-num="0" src="<?php the_post_thumbnail_url(); ?>">
                    <?php
                    $num = 1;
                    foreach ($attachment_ids as $attachment_id)
                    {
                        ?>
                        <img class="d-none" data-num="<?php echo $num ?>" src="<?php echo wp_get_attachment_image_url($attachment_id, 'original') ?>">
                        <?php
                        $num++;
                    }
                    ?>
                </div>

                <div class="act-div-right left-125p">
                    <span class="font-black good-single-title font-24"><?php the_title() ?></span>
                    <?php echo ( $sku = $product->get_sku() ) ? '<br><span class="font-light">'.esc_html_e("Артикул: ", "dverilending").$sku.'</span>' : '' ?>
                    <?php echo ( $product->is_in_stock() ) ? '<br><span class="font-light margin-bottom-20p">'.esc_html_e("В наличии", "dverilending").'</span>' : '<br><span class="font-light margin-bottom-20p">'.esc_html_e("Под заказ", "dverilending").'</span>' ?>
                    <div class="clearfix">
                        <div class="prices w-100p">
                            <?php
                            if(get_post_meta( get_the_ID(), '_price', true) != get_post_meta(get_the_ID(), '_regular_price', true)) {
                                ?>
                                <span class="old-price"><?php echo sprintf(__( '%s
                                    грн.', "dverilending"), get_post_meta(get_the_ID(), '_regular_price', true)) ?></span><br>
                                <?php
                            }
                            if(get_post_meta( get_the_ID(), '_price', true) != get_post_meta(get_the_ID(), '_regular_price', true)) {
                            ?>
                            <span class="act-price"><?php echo sprintf(__('%s<span class="font-regular font-24"> грн.</span>', "dverilending"), get_post_meta( get_the_ID(), '_price', true)) ?></span>
                            <?php }
                            else {
                                ?>
                                <span class="act-price"><?php echo sprintf(__('%s<span class="font-regular"> грн.</span>', "dverilending"), get_post_meta( get_the_ID(), '_regular_price', true))?></>
                            <?php
                            } ?>
                        </div>
                        <div class="act-buttons">
                        <div class="act-info float-left margin-top-25px">
                            <a style="color: white;"  data-toggle="modal" href="#buyWindow">
                                <span class="font-black px-35px font-24"><?php esc_html_e("Купить", "dverilending") ?></span>
                            </a>
                        </div>
                        <div class="act-coll-me margin-top-25px clearfix">
                            <a onclick="open_modal_call('product')" class="call-me" href="javascript: void (0)">
                                <span class="font-black font-18 recall_me_button"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_recall_me", __("Перезвоните мне", "dverilending")) ?></span>
                            </a>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="menu-product">
                            <div><a href="#blok3a_pseudo"><?php esc_html_e("смотреть описание", "dverilending") ?></a></div>
                            <div><a href="#how_buy"><?php esc_html_e("как купить", "dverilending") ?></a></div>
                            <div><a href="#block-10"><?php esc_html_e("о нас", "dverilending") ?></a></div>
                            <div><a href="#block-13"><?php esc_html_e("контакты", "dverilending") ?></a></div>
                        </div>
                        <div class="p-actions">
                            <div class="w-48p action">
                                <div class="icon-container d-inline-block"><img src="/wp-content/themes/dverilending/img/transport.png" class="icon fl-left"></div>
                                <div class="act-desc d-inline-block mt0" id="delivery_text"><?php echo ThemeCustomizer::get_theme_setting("mm_product_section_delivery_text", __("Бесплатная доставка за 120 минут", "dverilending")) ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="w-48p action">
                                <div class="icon-container d-inline-block"><img src="/wp-content/themes/dverilending/img/montaz.png" class="icon"></div>
                                <div class="act-desc d-inline-block" id="mounting_text"><?php echo ThemeCustomizer::get_theme_setting("mm_product_section_mounting_text", __("Бесплатный монтаж за 60 минут", "dverilending")) ?></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="next-button good"></span>
            <div class="clearfix"></div>
            <div class="small-act-imgs clearfix bottom-5p border-bottom" id="blok3a_pseudo">
                <img data-num="0" src="<?php the_post_thumbnail_url(); ?>">
                <?php
                $num = 1;
                foreach ($attachment_ids as $attachment_id)
                {
                    ?>
                    <img data-num="<?php echo $num ?>" src="<?php echo wp_get_attachment_image_url($attachment_id, 'original') ?>">
                <?php
                    $num++;
                }
                ?>
            </div>
        </div>
    </div>
    <div id=block-3a>
        <div class="container part-size text-center">
            <div class="w-30p">
                <div class="d-inline-block">
                    <h5 class="section-title text-left"><?php esc_html_e("Дверная коробка", "dverilending"); ?></h5>
                    <div class="pfields text-left">
                        <?php
                        foreach ($fields_sort['door_box'] as $field)
                        {
                            ?>
                            <div class="field">
                                <span class="field-label-custom"><?php echo $field['label'] ?>:</span>
                                <span class="field-value-custom"><?php echo $field['value'] ?></span>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="w-30p">
                <div class="d-inline-block">
                    <h5 class="section-title text-left"><?php esc_html_e("Дверное полотно", "dverilending"); ?></h5>
                    <div class="pfields text-left">
                        <?php
                        foreach ($fields_sort['door_pannel'] as $field)
                        {
                            ?>
                            <div class="field">
                                <span class="field-label-custom"><?php echo $field['label'] ?>:</span>
                                <span class="field-value-custom"><?php echo $field['value'] ?></span>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="w-30p">
                <div class="d-inline-block">
                    <h5 class="section-title text-left"><?php esc_html_e("Фурнитура двери", "dverilending"); ?></h5>
                    <div class="pfields text-left">
                        <?php
                        foreach ($fields_sort['door_forniture'] as $field)
                        {
                            ?>
                            <div class="field">
                                <span class="field-label-custom"><?php echo $field['label'] ?>:</span>
                                <span class="field-value-custom"><?php echo $field['value'] ?></span>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- block-3a -->
    <div id="block-8" class="good">
        <div class="container">
            <div class="block-8-man"></div>
            <div class="block-8-text" id="installation_for_60_min_content">
                <?php echo ThemeCustomizer::get_theme_setting("static_front_page_installation_for_60_min_content", __('<span>Бесплатный</span>
                <span>монтаж за 60 мин</span>
                <span>мастерами с опытом от 10 лет</span>
                <div>
                    По статистике 80%  договечности работы входных дверей зависит от установки. А это очень важный шаг, потому что плохой монтаж от неопытного "мастера" ничего хорошего не сулит и могут образоваться щели между дверью и коробом, будет продувание, слабая шумоизоляция, а также плохо работать замочная система.
                </div>', "dverilending")) ?>
            </div>
        </div>
    </div>
    <div id="block-9">
        <div class="container">
            <div>
                <h2 id="delivery_first_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_delivery_first_line", __('доставка по Киеву', "dverilending")) ?></h2>
                <h2 id="delivery_second_line"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_delivery_second_line", __('за 120 минут', "dverilending")) ?></h2>
            </div>
        </div>
    </div>
    <div id="block-10">
        <div class="container">
            <div>
                <h1 id="about_company_title"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_title", __('О компании ″викоцентр″', "dverilending")) ?></h1>
                <div class="orders clearfix">
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-1.png" alt=""><span id="about_company_first_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_first_text", __('15 лет с 2001', "dverilending")) ?></span></div>
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-2.png" alt=""><span id="about_company_second_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_second_text", __('600 моделей дверей', "dverilending")) ?></span></div>
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-3.png" alt=""><span id="about_company_third_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_third_text", __('2590 дверей на складе', "dverilending")) ?></span></div>
                    <div><img src="/wp-content/themes/dverilending/img/orders-icon-4.png" alt=""><span id="about_company_fourth_text"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_fourth_text", __('Установлено 7268 дверей', "dverilending")) ?></span></div>
                </div>
                <h3 id="about_company_year"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_year", __('за 2015 год', "dverilending")) ?></h3>
                <h4 id="about_company_doors_count"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_about_company_doors_count", __('1163 двери куплено в нашей компании по Киеву', "dverilending")) ?></h4>
            </div>
        </div>
    </div>
    <div id="block-12">
        <div class="container">
            <div class="manager-bg"></div>
            <div class="block-12-text">
                <div>
                    <?php _e('<h2>Есть вопрос?</h2>
                    <h2>Не можете определиться?</h2>
                    <div class="let-number">ОСТАВЬТЕ СВОЙ НОМЕР ТЕЛЕФОНА НАШ СПЕЦИАЛИСТ СВЯЖЕТСЯ С ВАМИ ЧЕРЕЗ 25 СЕКУНД И ПОМОЖЕТ С ВЫБОРОМ И КОНСУЛЬТАЦИЕЙ</div>', "dverilending"); ?>
                    <?php echo do_shortcode('[contact-form-7 id="160"]') ?>
                </div>
            </div>
        </div>
    </div>
    <div id="how_buy">
        <div class="container">
            <div class="text-center">
                <h5>Как купить?</h5>
                <div class="bactions">
                    <div class="w-23p text-center">
                        <div class="text-left">
                            <div class="text font-18 font-regular"><?php _e('Жмите<span class="hidden-xs"> кнопку</span> <span class="text-underline">КУПИТЬ</span>', 'dverilending') ?></div>
                            <div class="icon special-icon-how-buy-3"><img src="/wp-content/themes/dverilending/img/card.png"></div>
                            <div class="text font-18 font-regular hidden-xs"><?php _e('Жмите кнопку купить и вводите данные для заказа', 'dverilending') ?></div>
                        </div>
                    </div>
                    <div class="w-23p text-center">
                        <div class="text-left">
                            <div class="text font-18 font-regular"><?php _e('Вам позвонят', 'dverilending') ?></div>
                            <div class="icon special-icon-how-buy-4"><img src="/wp-content/themes/dverilending/img/call.png"></div>
                            <div class="text font-18 font-regular hidden-xs"><?php _e('В течении 25 секунд с вами связывается наш менеджер для подтверждения заказа', 'dverilending') ?></div>
                        </div>
                    </div>
                    <div class="w-23p text-center">
                        <div class="text-left">
                            <div class="text font-18 font-regular"><?php _e('Вам привезут', 'dverilending') ?></div>
                            <div class="icon special-icon-how-buy"><img src="/wp-content/themes/dverilending/img/transport-alternative.png"></div>
                            <div class="text font-18 font-regular hidden-xs" id="how_buy_delivery"><?php echo ThemeCustomizer::get_theme_setting("mm_product_section_how_buy_delivery", __('В течении 120 минут двери доставляют к вам по указаному адресу', "dverilending")) ?></div>
                        </div>
                    </div>
                    <div class="w-23p text-center">
                        <div class="text-left">
                            <div class="text font-18 font-regular"><?php _e('Вам установят', 'dverilending') ?></div>
                            <div class="icon special-icon-how-buy-2"><img src="/wp-content/themes/dverilending/img/montaz-alternative.png"></div>
                            <div class="text font-18 font-regular hidden-xs" id="how_buy_mounting"><?php echo ThemeCustomizer::get_theme_setting("mm_product_section_how_buy_mounting", __('Еще через 60 минут двери уже будут установлены', "dverilending")) ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="block-13">
        <div class="g-map">
            <div class="g-info">
                <div class="info">
                    <?php echo ThemeCustomizer::get_theme_setting("static_front_page_google_map_info", __('Приходите в наш салон по адресу:<br> ул. Гетьмана 10/37<br> сообщите менеджеру промокод:<br> "двери дешево"<br> и получите дополнительную скидку 5%', "dverilending")) ?>
                </div>
                <div class="phones">
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone1-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone1", __('+ 3 8 <span class="font-black">(098) 630-02-20</span>', "dverilending")) ?></a>
                    <a href="tel:<?php echo preg_replace("/[^\d\+]/", "", ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending"))) ?>" class="font-light font-14 phone2-link"><?php echo ThemeCustomizer::get_theme_setting("static_front_page_phone2", __('+ 3 8 <span class="font-black">(050) 630-02-20</span>', "dverilending")) ?></a>
                </div>
                <a href="<?php echo ThemeCustomizer::get_theme_setting("static_front_page_google_map_link", __('https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%92%D0%B0%D0%B4%D0%B8%D0%BC%D0%B0+%D0%93%D0%B5%D1%82%D1%8C%D0%BC%D0%B0%D0%BD%D0%B0,+10,+%D0%9A%D0%B8%D0%B5%D0%B2', "dverilending")) ?>" target="_blank" class="show-on-map" id="google_map_link">
                    <span><?php esc_html_e("Показать на карте где мы находимся", "dverilending"); ?></span></a>
            </div>
        </div>
    </div>

</div>


<!-- Modal -->
<div class="modal fade" id="buyWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="row">
                    <div class="w-37p icon">
                        <img src="<?php the_post_thumbnail_url(); ?>">
                    </div>
                    <div class="w-56p margin-left-10px p-relative">
                        <div class="font-20 font-light your-select"><?php esc_html_e("Ваш выбор", "dverilending") ?></div>
                        <h5 class="font-30 font-bold margin-top-10px margin-bottom-10px"><?php the_title() ?></h5>
                        <?php echo ( $sku = $product->get_sku() ) ? sprintf(__('<span class="art font-light font-20 margin-bottom-10px">Артикул: %s</span>', 'dverilending'), $sku) : '' ?>
                        <div class="price font-24 margin-top-25px margin-bottom-10px"><?php echo sprintf(__('<span class="font-bold">%s</span> грн.', 'dverilending'), get_post_meta( get_the_ID(), '_price', true))?></div>
                        <form action="<?php echo the_permalink() ?>" method="post">
                            <input type="text" class="font-24 w-100p margin-bottom-10px margin-top-10px" name="сname" placeholder="<?php esc_attr_e("Ваше имя", "dverilending") ?>">
                            <input type="text" class="font-24 w-100p margin-bottom-10px" name="phone" placeholder="<?php esc_attr_e("Ваш номер телефона", "dverilending") ?>">
                            <input type="text" class="font-24 w-100p margin-bottom-10px" name="address" placeholder="<?php esc_attr_e("Адрес доставки", "dverilending") ?>">
                            <button type="submit" name="submit_order" class="font-24 btn btn-primary buy-button w-100p"><?php esc_attr_e("КУПИТЬ", "dverilending") ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Modal -->
    <div class="modal fade" id="callWindow" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body text-center">
                    <h5 class="font-24 font-bold margin-top-10px margin-bottom-10px"><?php _e("Обратная связь", "dverilending") ?></h5>
                    <?php
                    echo do_shortcode('[contact-form-7 id="159"]');
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>

<?php do_action( 'woocommerce_after_single_product' ); ?>
<script type="text/javascript">
    function open_modal_call(place)
    {
        switch (place)
        {
            case 'head':
                jQuery('#callWindow [name=place]').val('Шапка. Напротив логотипа');
                break;
            case 'menu':
                jQuery('#callWindow [name=place]').val('Кнопка в меню');
                break;
            case 'product':
                jQuery('#callWindow [name=place]').val('Страница товара возле кнопки купить в первом блоке');
                break;
        }
        jQuery('#callWindow').modal('show');
        return false;
    }
    jQuery(function ($) {
        var product_current_slide = 0;
        var max_num_prod = <?php echo count($attachment_ids) ?>;
        $('#block-3 span.prev-button').click(function () {
            if(product_current_slide > 0)
            {
                $('#block-3 div.act-div-left img[data-num=' + product_current_slide + ']').addClass('d-none');
                product_current_slide--;
                $('#block-3 div.act-div-left img[data-num=' + product_current_slide + ']').removeClass('d-none');
            }
        });

        $('#callWindow input, #buyWindow input, #block-5 input, #block-12 input').focus(function () {
            var placeholder = $(this).attr('placeholder');
            $(this).attr('placeholder', '');
            $('#callWindow input, #buyWindow input, #block-5 input, #block-12 input').blur(function () {
                $(this).attr('placeholder', placeholder);
                $(this).off('blur');
            });
        });

        $('#block-3 span.next-button').click(function () {
            if(product_current_slide < max_num_prod)
            {
                $('#block-3 div.act-div-left img[data-num=' + product_current_slide + ']').addClass('d-none');
                product_current_slide++;
                $('#block-3 div.act-div-left img[data-num=' + product_current_slide + ']').removeClass('d-none');
            }
        });

        $('#block-3 div.small-act-imgs.clearfix img').click(function () {
            $('#block-3 div.act-div-left img[data-num=' + product_current_slide + ']').addClass('d-none');
            product_current_slide = $(this).data('num');
            $('#block-3 div.act-div-left img[data-num=' + product_current_slide + ']').removeClass('d-none');
        });

        var old_margin_top = $('#block-3').css('margin-top');
        old_margin_top = parseInt(old_margin_top.replace(/\D+/g,""));

        var margin_top = $('#product_main_menu').height() + old_margin_top;

        function sticky() {
            var y = $(window).scrollTop();
            var top = '0';

            if($('#wpadminbar').length > 0 && $(window).width() > 800)
            {
                top = $('#wpadminbar').height();
            }

            if( y > $('#wpadminbar').height() + 12 ){
                $('#product_main_menu').css({
                    'position': 'fixed',
                    'top': top,
                    'width': $('#block-3').width(),
                    'background': '#fff',
                    'z-index': '9000',
                });
                $('#block-3').css('margin-top', margin_top + 'px');
            } else {
                $('#product_main_menu').removeAttr('style');
                $('#block-3').css('margin-top', old_margin_top + 'px');
            }
        }
        sticky();
        $(window).scroll(sticky);
        $(window).resize(sticky);
    })
</script>
